<?php session_start();

   if (!isset($_SESSION['login'])) 
   { 
     header('Location:signIn.php');
  }
   require 'headers.php';

   $user_id = $_SESSION['id'];
   $user_address= $_SESSION['address'];
  $user_address_type= $_SESSION['address_type'];


         
  
 ?>
 <!DOCTYPE html>
<html lang="en">

    <div class="hero-wrap hero-bread" style="background-image: url('images/banner/bg_1.jpg');">
      <div class="container">
        <div class="row no-gutters slider-text align-items-center justify-content-center">
          <div class="col-md-9 ftco-animate text-center">
            <p class="breadcrumbs"><span class="mr-2"><a href="index.html">Home</a></span> <span>Address</span></p>
            <h1 class="mb-0 bread">Add Address</h1>
          </div>
        </div>
      </div>
    </div>


    <section class="ftco-section ftco-cart">
      <div class="container">
        <div class="row">

          <div class="col-md-12 ftco-animate">
            <div class="cart-list">
              <table class="table">
                <thead class="thead-primary">
                  <tr class="text-center">
                    <th>&nbsp;</th>
                    <!-- <th>&nbsp;</th> -->
                    <th>Address</th>
                    <th>Type</th>
                    <th>Edit</th>
                    <th>&nbsp;</th>
                  </tr>
                </thead>
                
                <tbody>

                  <tr class="text-center">

                     <td class="empty">
                    </td>
                    
                    
                    <td class="Address">
                      <h3><?= $user_address ?></h3>
                    </td>
                    <td class="Type">
                      <h3><?= $user_address_type ?></h3>
                    </td>
                    
                    <td class="Edit"><a href="Profile.php" class="btn-Success">Edit</a></td>
                    <td></td>
                    
                    
                  </tr>

                  
                </tbody>
                


                <?php

                 include 'buy/db.php';

                $queryfirst = "SELECT * FROM userAddress WHERE  UserId = $user_id";
                $resultfirst = $connection->query($queryfirst);
               /* echo $queryfirst;
                echo isset($resultfirst->num_rows);
                print_r($resultfirst->num_rows);
                //die;*/
                if (isset($resultfirst->num_rows)) {
                  // output data of each row
                  $count=1;
                  while($rowfirst = $resultfirst->fetch_assoc()) {
                    $NoOfAddresses = $rowfirst['NoOfAddresses'];
                     while($NoOfAddresses >= $count )

                     {
                        $addresskey='Address'.$count;
                        $typeKey='Type'.$count;
                        $Address = $rowfirst[$addresskey];
                        $Type = $rowfirst[$typeKey];
                                                                          //if NoOfAddresses is not showing correct address
                                                                         //then code will give an error.
                        
           

            ?>
                <tbody>

                  <tr class="text-center">
                    <?php if ($NoOfAddresses == $count)
                    { ?>
                    <td class="product-remove"><a href="addressRemove.php/?id=<?=  $count ?>"><span class="ion-ios-close"></span></a></td>
                  <?php }else
                  {
                     echo '<td></td>';
                  } 
                  ?>
                    
                    
                    
                    <td class="Address">
                      <h3><?= $Address ?></h3>
                    </td>
                    <td class="Type">
                      <h3><?= $Type ?></h3>
                    </td>
                    
                    <td class="Edit"><a href="AddressEdit.php?Id=<?= $count; ?>" class="btn-Success">Edit</a></td>
                    <td></td>
                    
                    
                  </tr>

                  
                </tbody>
                <?php
                $count = $count +1;                   
                 }}} ?>
              </table>
            </div>
          </div>
           
        </div>
       
      </div>
    </section>

    <section class="ftco-section">
      <div class="container">
        <div class="row justify-content-center">
          <div class="col-xl-7 ftco-animate">
             <form action="addAddressData.php" method="post"  enctype="multipart/form-data">
              <div class="row align-items-end">
                <div class="w-100"></div>


                <div class="col-md-6">
                  <div class="form-group">
                    <label for="streetaddress">Street Address</label>
                    <input type="text" class="form-control" name="address" placeholder="House number and street name"  required="" >
                  </div>
                

                <div class="form-group">
                    <div class="col-md-3">
                      <div class="radio">
                         <label><input type="radio" name="addresstype" required="" class="mr-2" value="home"> Home</label>
                      </div>
                    </div>
                  </div>
                  
                  <div class="form-group">
                    <div class="col-md-3">
                      <div class="radio">
                         <label><input type="radio" name="addresstype" required="" class="mr-2" value="work"> Work</label>
                      </div>
                    </div>
                  </div>

                  <div class="form-group">
                    <div class="col-md-3">
                      <div class="radio">
                         <label><input type="radio" name="addresstype" required="" class="mr-2" value="other"> Other</label>
                      </div>
                    </div>
                  </div>
                </div>
                  


    
                
                <div class="w-100"></div>
                <div class="col-md-12">
                  <div class="form-group mt-4">
                  <button type="submit" class="btn btn-info"  name ="signUp" style="width: 120px; height: 38px"> Update Profile</button>
                  </div>
                </div>
              </div>
            </form><!-- END -->
          </div>
          </div>
          
        </div>
      </div>
    </section> <!-- .section -->



    

    
    <?php  require 'footer.php'; ?>
  
    
  </body>
</html>