<?php session_start(); 
  if (!isset($_SESSION['login'])) 
   { 
     header('Location: signIn.php');
  }
   require 'headers.php';
        $id =$_GET['Id'];

                 include 'buy/db.php';

                $queryfirst = "SELECT * FROM userAddress WHERE  UserId = $user_id LIMIT 1";
                $resultfirst = $connection->query($queryfirst);
              
                if (isset($resultfirst->num_rows)) {
                  
                  
                  while($rowfirst = $resultfirst->fetch_assoc()) {
                    
                     
                        $addresskey='Address'.$id;
                        $typeKey='Type'.$id;
                        $Address = $rowfirst[$addresskey];
                        $Type = $rowfirst[$typeKey];

                      }}
                                                                          
                    


         
  
 ?>
  
 <!DOCTYPE html>
<html lang="en">
    <div class="hero-wrap hero-bread" style="background-image: url('images/banner/bg_1.jpg');">
      <div class="container">
        <div class="row no-gutters slider-text align-items-center justify-content-center">
          <div class="col-md-9 ftco-animate text-center">
            <p class="breadcrumbs"><span class="mr-2"><a href="index.html">Home</a></span> <span>Address</span></p>
            <h1 class="mb-0 bread">Edit Address</h1>
          </div>
        </div>
      </div>
    </div>


    

    <section class="ftco-section">
      <div class="container">
        <div class="row justify-content-center">
          <div class="col-xl-7 ftco-animate">
             <form action="editAddress.php?id=<?= $id ?>" method="post"  enctype="multipart/form-data">
              <div class="row align-items-end">
                <div class="w-100"></div>


                <div class="col-md-6">
                  <div class="form-group">
                    <label for="streetaddress">Street Address</label>
                    <input type="text" class="form-control" name="address" value="<?php echo $Address; ?>"   required="" >
                  </div>
                

                <div class="form-group">
                    <div class="col-md-3">
                      <div class="radio">
                         <label><input type="radio" name="addresstype" required="" class="mr-2" value="home"> Home</label>
                      </div>
                    </div>
                  </div>
                  
                  <div class="form-group">
                    <div class="col-md-3">
                      <div class="radio">
                         <label><input type="radio" name="addresstype" required="" class="mr-2" value="work"> Work</label>
                      </div>
                    </div>
                  </div>

                  <div class="form-group">
                    <div class="col-md-3">
                      <div class="radio">
                         <label><input type="radio" name="addresstype" required="" class="mr-2" value="other"> Other</label>
                      </div>
                    </div>
                  </div>
                </div>
                  


    
                
                <div class="w-100"></div>
                <div class="col-md-12">
                  <div class="form-group mt-4">
                  <button type="submit" class="btn btn-info"  name ="signUp" style="width: 120px; height: 38px"> Update Address</button>
                  </div>
                </div>
              </div>
            </form>
          </div>
          </div>
          
        </div>
      </div>
    </section> <!-- .section -->



    

    
    <?php  require 'footer.php'; ?>
  
    
  </body>
</html>