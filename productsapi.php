<?php
include 'buy/db.php';

    $request_method=$_SERVER["REQUEST_METHOD"];
    switch($request_method)
    {
        case 'GET':
            // Retrive Products
            if(!empty($_GET["product_id"]))
            {
                $product_id=intval($_GET["product_id"]);
                get_products($product_id);
            }
						else if(!empty($_GET["category_id"]))
            {
                $category_id=intval($_GET["category_id"]);
                get_category($category_id);
            }
            else
            {
                get_products();
            }
            break;




        case 'POST':
            if(empty($_POST["product_name"]))
            {
                 $response=array(
                'status' => 402,
                'status_message' =>'product name is required.');


                header('Content-Type: application/json');
                echo json_encode($response);
                break;
            }


            elseif(empty($_POST["price"]))
            {
                 $response=array(
                'status' => 402,
                'status_message' =>'price is required.');


                header('Content-Type: application/json');
                echo json_encode($response);
                break;
            }

            elseif(empty($_POST["sellPrice"]))
            {
                 $response=array(
                'status' => 402,
                'status_message' =>'selling Price is required.');


                header('Content-Type: application/json');
                echo json_encode($response);
                break;
            }

            elseif(empty($_POST["description"]))
            {
                 $response=array(
                'status' => 402,
                'status_message' =>'description is required.');


                header('Content-Type: application/json');
                echo json_encode($response);
                break;
            }


            elseif(empty($_POST["category"]))
            {
                 $response=array(
                'status' => 402,
                'status_message' =>'category is required.');


                header('Content-Type: application/json');
                echo json_encode($response);
                break;
            }

            elseif(empty($_POST["thumbnail"]))
            {
                 $response=array(
                'status' => 402,
                'status_message' =>'thumbnail is required.');


                header('Content-Type: application/json');
                echo json_encode($response);
                break;
            }


            else
            {
                insert_product();
                break;
            }






        case 'PUT':
             if(empty($_GET["product_name"]))
            {
                 $response=array(
                'status' => 402,
                'status_message' =>'product name is required.');


                header('Content-Type: application/json');
                echo json_encode($response);
                break;
            }


            elseif(empty($_GET["price"]))
            {
                 $response=array(
                'status' => 402,
                'status_message' =>'price is required.');


                header('Content-Type: application/json');
                echo json_encode($response);
                break;
            }

            elseif(empty($_GET["sellPrice"]))
            {
                 $response=array(
                'status' => 402,
                'status_message' =>'selling Price is required.');


                header('Content-Type: application/json');
                echo json_encode($response);
                break;
            }

            elseif(empty($_GET["description"]))
            {
                 $response=array(
                'status' => 402,
                'status_message' =>'description is required.');


                header('Content-Type: application/json');
                echo json_encode($response);
                break;
            }

            elseif(empty($_GET["product_id"]))
            {
                 $response=array(
                'status' => 402,
                'status_message' =>'product id is required.');


                header('Content-Type: application/json');
                echo json_encode($response);
                break;
            }

            elseif(empty($_GET["category"]))
            {
                 $response=array(
                'status' => 402,
                'status_message' =>'category is required.');


                header('Content-Type: application/json');
                echo json_encode($response);
                break;
            }


           

            elseif(empty($_GET["thumbnail"]))
            {
                 $response=array(
                'status' => 402,
                'status_message' =>'thumbnail is required.');


                header('Content-Type: application/json');
                echo json_encode($response);
                break;
            }




            else
            {
                update_product();
                break;
            }



        case 'DELETE':
            if(!empty($_GET["product_id"]))
            {
                $product_id=intval($_GET["product_id"]);
                delete_product($product_id);
            }
            else
            {
                $response=array(
                'status' => 402,
                'status_message' =>'product id is required.');


                header('Content-Type: application/json');
                echo json_encode($response);
                break;
            }
        default:
            // Invalid Request Method
            header("HTTP/1.0 405 Method Not Allowed");
            break;
    }



    function get_products($product_id=0)
    {
        global $connection;
        $query="SELECT * FROM product";
        if($product_id != 0)
        {
            $query.=" WHERE id=".$product_id;
        }
        $response=array();
       // $resultfirst = $connection->query($queryfirst);
        $result=mysqli_query($connection, $query);
        while($row=$result->fetch_assoc())
        {
           // print_r($row);die;
            $response[]=$row;
        }
        header('Content-Type: application/json');
        echo json_encode($response);
    }

		function get_category($category_id=0)
    {
        global $connection;
				
        $query="SELECT * FROM product";
        if($category_id != 0)
        {
            $query.=" WHERE category=".$category_id;
        }
        $response=array();
       // $resultfirst = $connection->query($queryfirst);
			 
        $result = mysqli_query($connection, $query);
        while($row=$result->fetch_assoc())
        {
           // print_r($row);die;
            $response[]=$row;
        }
        header('Content-Type: application/json');
        echo json_encode($response);
    }

    function insert_product()
    {
        global $connection;
        $product_name=$_POST["product_name"];
        $marketPrice=$_POST["price"];
        $sellPrice=$_POST["sellPrice"];
        $description=$_POST["description"];
        $category=$_POST["category"];
        $thumbnail=$_POST["thumbnail"];
        $status = 100-(round($sellPrice/$marketPrice,2) * 100);
        $status = $status."%";

        $query="INSERT INTO product SET name='{$product_name}', price={$marketPrice}, sellPrice={$sellPrice}, description='{$description}', thumbnail='{$thumbnail}',category={$category} , status = '{$status}'";
        if(mysqli_query($connection, $query))
        {
            $response=array(
                'status' => 200,
                'status_message' =>'Product Added Successfully.'
            );
        }
        else
        {
            $response=array(
                'status' => 500,
                'status_message' =>'Product Addition Failed.'
            );
        }
        header('Content-Type: application/json');
        echo json_encode($response);
    }




    function update_product($product_id)
    {
        global $connection;
        parse_str(file_get_contents("php://input"),$post_vars);
        $product_name=$post_vars["product_name"];
        $price=$post_vars["price"];
        $quantity=$post_vars["quantity"];
        $seller=$post_vars["seller"];
        $query="UPDATE products SET product_name='{$product_name}', price={$price}, quantity={$quantity}, seller='{$seller}' WHERE id=".$product_id;
        if(mysqli_query($connection, $query))
        {
            $response=array(
                'status' => 1,
                'status_message' =>'Product Updated Successfully.'
            );
        }
        else
        {
            $response=array(
                'status' => 0,
                'status_message' =>'Product Updation Failed.'
            );
        }
        header('Content-Type: application/json');
        echo json_encode($response);
    }



    function delete_product($product_id)
    {
        global $connection;
        $query="DELETE FROM product WHERE id=".$product_id;
        if(mysqli_query($connection, $query))
        {
            $response=array(
                'status' => 1,
                'status_message' =>'Product Deleted Successfully.'
            );
        }
        else
        {
            $response=array(
                'status' => 0,
                'status_message' =>'Product Deletion Failed.'
            );
        }
        header('Content-Type: application/json');
        echo json_encode($response);
    }


    



    ?>