<?php session_start(); ?> 
<!DOCTYPE html>
<html lang="en">
<style type="text/css">
  hr {
  border-style: double;
  border-width: 2px;
}
</style>

<?php  
  require 'headers.php'; 
 if (isset($_SESSION['login'])) {
    
    ?>
    <section id="home-section" class="hero">
      <div class="home-slider owl-carousel">
        <div class="slider-item" style="background-image: url(images/banner/bg_1.jpg">
          <div class="overlay"></div>
          <div class="container">
            <div class="row slider-text justify-content-center align-items-center" data-scrollax-parent="true">

              <div class="col-md-12 ftco-animate text-center">
                <h1 class="mb-2">You Have Already Login</h1>
                <h2 class="subheading mb-4">We deliver organic vegetables &amp; fruits</h2>
                <p><a href="index.php" class="btn btn-primary">Go To Home</a></p>
              </div>

            </div>

          </div>

        </div>
      </div>
    </section>
    <?php
  }
  else
  {
?>

  <hr size="2px">

    <section class="ftco-section">
      <div class="container">
        <div class="row justify-content-center">
          <div class="col-xl-7 ftco-animate">
            <form action="signInData.php" method="post"  enctype="multipart/form-data">
              <h3 class="mb-4 billing-heading">Login</h3>
              <div class="row align-items-end">
                <div class="col-md-6">
                  <div class="form-group">
                    <label for="firstname">Email</label>
                    <input type="text" class="form-control" name="email" placeholder="Email" required="">
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="form-group">
                    <label for="lastname">Password</label>
                    <input type="Password" class="form-control" name="Password" placeholder="Password" required="">
                  </div>
                </div>
                <div class="col-md-12" >
                  <div class="form-group mt-4">
                  <button type="submit" class="btn btn-info" style="width: 100; height: 32px">Sign In</button>
                  <a href="signUp.php" class="btn btn-info" style="margin-left: 20px;width: 100; height: 32px;text-align: center;">Sign Up</a>
                  </div>

                </div>
                <div style="margin-left: 3%; color: red; font-size: 2vw;" >
                <?php 
                if (isset($_GET['message']))
                  { 
                       echo $_GET['message'];  
                   }
                ?>
                </div>
              </div>
            </form><!-- END -->
          </div>
        </div>
      </div>
    </section> <!-- .section -->

    <?php
      }
      require 'footer.php'; ?>
    
  </body>
</html>