<?php session_start();  
  require 'headers.php'; 
  $id =$_GET['id'];
  ?>
  <!DOCTYPE html>
<html lang="en">

    <div class="hero-wrap hero-bread" style="background-image: url('images/banner/bg_1.jpg');">
      <div class="container">
        <div class="row no-gutters slider-text align-items-center justify-content-center">
          <div class="col-md-9 ftco-animate text-center">
          	<b><p class="breadcrumbs"><span class="mr-2"><a href="index.html">Home</a></span> <span>Category</p></b>
            <h1 class="mb-0 bread">Category</h1>
          </div>
        </div>
      </div>
    </div>

    <section class="ftco-section">
    	<div class="container">

                 <section class="ftco-section">
        <div class="container">
                <div class="row justify-content-center mb-3 pb-3">
          <div class="col-md-12 heading-section text-center ftco-animate">
            <h2 class="mb-4">Categorise Products</h2>
          </div>
        </div>          
        </div>



        <div class="container">
            <div class="row">
           <?php

                 include 'buy/db.php';

                $queryfirst = "SELECT * FROM product where category=$id ";
                $resultfirst = $connection->query($queryfirst);
                if ($resultfirst->num_rows > 0) {
                  // output data of each row
                  while($rowfirst = $resultfirst->fetch_assoc()) {

                        $id_best = $rowfirst['id'];
                        $name_best = $rowfirst['name'];
                        $price_best = $rowfirst['price'];
                        $sellingPrice = $rowfirst['sellPrice'];
                        $status = $rowfirst['status'];
                        $Description = $rowfirst['description'];
                        $thumbnail_best = $rowfirst['thumbnail'];
           

            ?>
                <div class="col-md-6 col-lg-3 ftco-animate">
                    <div class="product">
                        <a href="product-single.php?id=<?= $id_best; ?>" class="img-prod"><img class="img-fluid" src="images/products/<?= $thumbnail_best; ?>" alt="Colorlib Template">
                            <span class="status"><?= $status; ?></span>
                            <div class="overlay"></div>
                        </a>
                        <div class="text py-3 pb-4 px-3 text-center">
                            <h3><a href="#"><?= $name_best; ?></a></h3>
                            <div class="d-flex">
                                <div class="pricing">
                                    <p class="price"><span class="mr-2 price-dc">$ <?= $price_best; ?></span><span class="price-sale">$ <?= $sellingPrice; ?></span></p>
                                </div>
                            </div>
                            <div class="bottom-area d-flex px-3">
                                <div class="m-auto d-flex">
                                    
                                    <a href="product-single.php?id=<?= $id_best; ?>" class="buy-now d-flex justify-content-center align-items-center mx-1">
                                        <span><i class="ion-ios-cart"></i></span>
                                    </a>
                                  
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
           <?php }} ?>
            </div>
        </div>
     
    </section> 
    		<div class="row mt-5">
          <div class="col text-center">
            <div class="block-27">
              <ul>
                <li><a href="#">&lt;</a></li>
                <li class="active"><span>1</span></li>
                <li><a href="#">2</a></li>
                <li><a href="#">3</a></li>
                <li><a href="#">4</a></li>
                <li><a href="#">5</a></li>
                <li><a href="#">&gt;</a></li>
              </ul>
            </div>
          </div>
        </div>
    	</div>
    </section>

		
<?php  require 'footer.php'; ?>
    
  </body>
</html>