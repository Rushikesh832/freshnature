<?php session_start(); 

   if (!isset($_SESSION['login'])) 
   { 
     header('Location: signIn.php');
  }
  if ($_SESSION['role'] != 'admin') {
  header('Location: ../../index.php');

}
   require '../headers.php';


         
  
 ?>

 <!DOCTYPE html>
<html lang="en">
     


    <section class="ftco-section ftco-cart">
      <div class="container">
        <div class="row justify-content-center mb-3 pb-3">
          <div class="col-md-12 heading-section text-center ftco-animate">
            <h2 class="mb-4">All Orders</h2>
            <div style="text-align: right;">
            <a href="ExportOrders.php" class="btn btn-info" style="width: 100; height: 40px;text-align: center;"><h4>Export Orders</h4></a>
            </div>
          </div>
        </div>      
      </div>
      <div class="container">
        <div class="row">

          <div class="col-md-12 ftco-animate">
            <div class="cart-list">
              <table class="table">
                <thead class="thead-primary">
                  <tr class="text-center">
                    
                    <th>E-mail</th>
                    <th>First Name</th>
                    <th>Last Name</th>
                    <th>Name</th>
                   <th> Total</th>
                       <th> Quantity</th>
                      <th> RazorpayId</th>
                      <th>OrderPlacedTime</th>
                     
                    
                    <th> Address</th>
                    <th>Address type</th>
                    
                    <th>City</th>

                    <th>Country</th>
                    <th>Status</th>
                     <th>Delivery Slot</th>
                     <th>Delivery Date</th>
                     <th>Preview</th>
                    
                    
                  </tr>
                </thead>
                <?php

                 include '../db.php';
                  $queryfirst = "SELECT * FROM Orders, users, OrderDetails,product where users.Id=Orders.UserId AND Orders.OrderId=OrderDetails.OrderId AND product.id=OrderDetails.ProductId";
                 // echo $queryfirst;
                $resultfirst = $connection->query($queryfirst);
                if (isset($resultfirst->num_rows)) {
                  while($rowfirst = $resultfirst->fetch_assoc()) {



                        $OrderId = $rowfirst['OrderId'];
                        $email = $rowfirst['email'];
                        $firstname = $rowfirst['firstname'];
                        $lastname = $rowfirst['lastname'];
                        $address = $rowfirst['address'];
                        $addressType = $rowfirst['addressType'];
                        $city = $rowfirst['city'];
                        $country = $rowfirst['country'];
                        $Total = $rowfirst['Total'];
                        $Quantity = $rowfirst['Quantity'];
                        $RazorpayId = $rowfirst['RazorpayId'];
                        $ProductId = $rowfirst['ProductId'];
                        $name = $rowfirst['name'];
                        $Status = $rowfirst['Status'];
                        $DeliverySlot = $rowfirst['DeliverySlot'];
                        $DeliveryDate = $rowfirst['DeliveryDate'];
                        $OrderPlacedTime = $rowfirst['OrderPlacedTime'];
                         
                        
                        
           
                        
           

            ?>
                <tbody>

                  <tr class="text-center" >
                   
                  
                    <td class="email" style="width:50px">
                      <h3 ><?= $email ?></h3>
                    </td>
                    <td class="firstname">
                      <h3><?= $firstname ?></h3>
                    </td><td class="lastname">
                      <h3><?= $lastname ?></h3>
                    </td>
                     <td class="Name">
                      <h3><?= $name ?></h3>
                       </td>
                    <td class="Total">
                      <h3><?= $Total ?></h3>
                       </td>
                       <td class="Quantity">
                      <h3><?= $Quantity ?></h3>
                      </td><td class="RazorpayId">
                      <h3><?= $RazorpayId ?></h3>
                      </td><td class="OrderPlacedTime">
                      <h3><?= $OrderPlacedTime ?></h3>
                      
                    </td><td class="address">
                      <h3><?= $address ?></h3>
                    </td><td class="addressType">
                      <h3><?= $addressType ?></h3>
                    </td>
                     <td class="city">
                      <h3><?= $city ?></h3>
                    </td>
                    
                    <td class="country">
                      <h3><?= $country ?></h3>
                    </td>

                   

                    <td class="Status">
                       <?php  if ($Status == 'new') {
                            ?>
                            <h3>
                            <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" href="#" id="dropdown04" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">NEW</a>

                            <div class="dropdown-menu" aria-labelledby="dropdown04">

                              <a class="dropdown-item" href="ChangeStatus.php?status=progress&Orderid=<?= $OrderId ?>">Progress</a>
                              <a class="dropdown-item" href="ChangeStatus.php?status=complete&Orderid=<?= $OrderId ?>">Complete</a>

                            </div>
                            </li>
                           </h3>
                            <?php

                            }  elseif($Status == 'progress') {
                            ?>
                            <h3>
                            <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" href="#" id="dropdown04" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            Progress</a>

                            <div class="dropdown-menu" aria-labelledby="dropdown04">
                            <a class="dropdown-item" href="ChangeStatus.php?status=complete&Orderid=
                            <?= $OrderId ?>">Complete</a>

                            </div>
                            </li>
                           </h3>
                            <?php

                            }
                            else  {
                            ?>
                            <h3>Complete
                           </h3>
                            <?php

                            } ?>
                          
                    </td>
                    
										<td class="DeliverySlot">
                      <h3><?= $DeliverySlot ?></h3>
                    </td>
										<td class="DeliveryDate">
                      <h3><?= $DeliveryDate ?></h3>
                    </td>

                    <td class="Preview"><a href="OrderPreview.php?Id=<?= $OrderId; ?>" class="btn-Success">Preview</a></td>
                    
                    
                
                    <td></td>
                    
                    
                  </tr>

                  
                </tbody>
                <?php
                }} ?>
              </table>
            </div>
          </div>
           
        </div>
       
      </div>
    </section>

    



    

    
    <?php  require '../footer.php'; ?>
  
    
  </body>
</html>