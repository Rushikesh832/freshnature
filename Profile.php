<?php session_start(); 
     if (!isset($_SESSION['login'])) 
   { 
     header('Location:signIn.php');
  }
  
require 'headers.php';

   					$user_id =$_SESSION['id'] ;
				    $user_firstname = $_SESSION['firstname'] ;
				    $user_lastname  =  $_SESSION['lastname'];
				    $user_address = $_SESSION['address'] ;
				    $user_city =  $_SESSION['city'] ;
				    $user_country =  $_SESSION['country'] ;
				    $user_email =  $_SESSION['email'] ;
  
 ?>
<!DOCTYPE html>
<html lang="en">
    <div class="hero-wrap hero-bread" style="background-image: url('images/banner/bg_1.jpg');">
      <div class="container">
        <div class="row no-gutters slider-text align-items-center justify-content-center">
          <div class="col-md-9 ftco-animate text-center">
          	<p class="breadcrumbs"><span class="mr-2"><a href="index.html">Home</a></span> <span>Profile</span></p>
            <h1 class="mb-0 bread">Profile</h1>
          </div>
        </div>
      </div>
    </div>

    <section class="ftco-section">
      <div class="container">
        <div class="row justify-content-center">
          <div class="col-xl-7 ftco-animate">
						 <form action="UpdateUserProfile.php" method="post"  enctype="multipart/form-data">
              <div class="row align-items-end">
                <div class="col-md-6">
                  <div class="form-group">
                    <label for="firstname">First Name</label>
                    <input type="text" name="firstname" class="form-control"  required="" value="<?php echo $user_firstname?>"  >
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="form-group">
                    <label for="lastname">Last Name</label>
                    <input type="text"  name="lastname" class="form-control" value="<?php echo $user_lastname?>"  required="">
                  </div>
                </div>
                <div class="w-100"></div>
                <div class="col-md-6">
                  <div class="form-group">
                    <label for="country">Country</label>
                    <input type="text"  name="country" class="form-control" value="<?php echo $user_country?>" required="" >
                  </div>
                </div>
                 <div class="col-md-6">
                  <div class="form-group">
                    <label for="towncity">Town / City</label>
                    <input type="text" class="form-control" name="city" value="<?php echo $user_city?>" required="" >
                  </div>
                </div>
                <div class="w-100"></div>
                <div class="col-md-12">
                  <div class="form-group">
                    <label for="streetaddress">Street Address</label>
                    <input type="text" class="form-control" name="address" value="<?php echo $user_address?>"  required="" >
                  </div>
                </div>
               

                 <div class="form-group">
                    <div class="col-md-3">
                      <div class="radio">
                         <label><input type="radio" name="addresstype" required="" class="mr-2" value="home"> Home</label>
                      </div>
                    </div>
                  </div>
                  
                  <div class="form-group">
                    <div class="col-md-3">
                      <div class="radio">
                         <label><input type="radio" name="addresstype" required="" class="mr-2" value="work"> Work</label>
                      </div>
                    </div>
                  </div>

                  <div class="form-group">
                    <div class="col-md-3">
                      <div class="radio">
                         <label><input type="radio" name="addresstype" required="" class="mr-2" value="other"> Other</label>
                      </div>
                    </div>
                  </div>
                <div class="w-100"></div>
                <div class="col-md-12">
                  <div class="form-group mt-4">
                  <button type="submit" class="btn btn-info"  name ="signUp" style="width: 120px; height: 38px"> Update Profile</button>
                  </div>
                </div>
              </div>
            </form><!-- END -->
          </div>
					</div>
					
        </div>
      </div>
    </section> <!-- .section -->

		
    <?php  require 'footer.php'; ?>
  
    
  </body>
</html>