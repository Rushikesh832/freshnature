<?php session_start(); 
    require 'headers.php';
    $user_id = $_SESSION['id'];


 ?>

<!DOCTYPE html>
<html lang="en">
    <div class="hero-wrap hero-bread" style="background-image: url('images/banner/bg_1.jpg');">
      <div class="container">
        <div class="row no-gutters slider-text align-items-center justify-content-center">
          <div class="col-md-9 ftco-animate text-center">
            <p class="breadcrumbs"><span class="mr-2"><a href="index.html">Home</a></span> <span>Gallary</span></p>
            <h1 class="mb-0 bread">Gallary Section</h1>
          </div>
        </div>
      </div>
    </div>

    <section class="ftco-section">
      <div class="container">
        <div class="row justify-content-center mb-3 pb-3">
          <div class="col-md-12 heading-section text-center ftco-animate">
            <span class="subheading"><h2>Gallary</h2></span>
          </div>
        </div>      
      </div>



      <div class="container">
        <div class="row">
           <?php

                 include 'buy/db.php';

                $queryfirst = "SELECT * FROM Gallary limit 8";
                $resultfirst = $connection->query($queryfirst);
                if ($resultfirst->num_rows > 0) {
                  // output data of each row
                  while($rowfirst = $resultfirst->fetch_assoc()) {

                        $id_best = $rowfirst['Id'];
                        $name_best = $rowfirst['name'];
                        $Description = $rowfirst['Description'];
                        $thumbnail_best = $rowfirst['gallaryPicture'];
           

            ?>
          <div class="col-md-6 col-lg-3 ftco-animate">
            <div class="product">
              <a href="#" class="img-prod"><img height="200px" width="400px" class="img-fluid" src="images/gallary/<?= $thumbnail_best; ?>" alt="Colorlib Template">
              </a>
              <div class="text py-3 pb-4 px-3 text-center">
                <h2><a href="#"><?= $name_best; ?></a></h2>
              </div>
              <h4><?= $Description; ?></h4>
            </div>
          </div>
           <?php }} ?>
        </div>
      </div>
     
    </section> 



    

    <?php

     
      require 'footer.php'; ?>
    }
    
  </body>
</html>