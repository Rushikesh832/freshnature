<?php
include 'buy/db.php';

    $request_method=$_SERVER["REQUEST_METHOD"];
    switch($request_method)
    {
        case 'GET':
           if(!empty($_GET["cart_id"]))
            {
                $cart_id=intval($_GET["cart_id"]);
                delete_cart($cart_id);
            }
			else
			{
				   $response=array(
                    'status' => 402,
                    'status_message' =>'user_id is required.');


                    header('Content-Type: application/json');
                    echo json_encode($response);
			}
            break;
       

            
        case 'POST':
           
                if(empty($_POST["user_id"]))
                {
                     $response=array(
                    'status' => 402,
                    'status_message' =>'user_id is required.');


                    header('Content-Type: application/json');
                    echo json_encode($response);
                    break;
                }
                else
                {

                    delete_cart_by_user_id($_POST["user_id"]);
                }
            
            break;
        default:
            // Invalid Request Method
             $response=array(
                'status' => 402,
                'status_message' =>'Request Not Allowed.');


                header('Content-Type: application/json');
                echo json_encode($response);
            break;
    }



    function get_cart($user_id)
    {
        global $connection;
        $query="SELECT product.*,Cart.*,category.name as category_name ,product.name as product_name FROM Cart,product,category WHERE Cart.ProductId = product.id and product.category = category.Id and Cart.UserId= $user_id";
        //echo $query;
        $response=array();
       // $resultfirst = $connection->query($queryfirst);
        $result=mysqli_query($connection, $query);
        if($result->num_rows)
        {


            while($row=$result->fetch_assoc())
            {
               // print_r($row);die;
                $response[]=$row;
            }
        }
        else
        {
            $response=array(
                                'status' => 200,
                                'status_message' =>'empty Cart.'
                            );
        }
        

        
        header('Content-Type: application/json');
        echo json_encode($response);
    }



    function insert_cart()
    {
        global $connection;
        $ProductId=$_POST["product_id"];
        $Quantity=$_POST["quantity"];
        $UserId=$_POST["user_id"];


        $query ="SELECT * from Cart where UserId = {$UserId} and ProductId = {$ProductId} Limit 1";
        $result=mysqli_query($connection, $query);
        if($result->num_rows)
        {
            $qty=0;
            while($row=$result->fetch_assoc())
                {
                   $qty = $Quantity + $row['Quantity'];
                   $id  = $row['CartId'];

                   if($qty == 0)
                   {
                        $Deletequery="DELETE FROM Cart  where CartId=".$id;

                        if(mysqli_query($connection, $Deletequery))
                        {
                            $response=array(
                                'status' => 200,
                                'status_message' =>'Product Deleted Successfully In Cart.'
                            );
                        }
                        else
                        {
                            $response=array(
                                'status' => 500,
                                'status_message' =>'Product Deletion Failed.'
                            );
                        }

                   }
                    elseif($qty < 0)
                   {
                             $response=array(
                                'status' => 200,
                                'status_message' =>"Quantity Cannot be Negative."
                            );
                   }
                   else
                   {



                       $Insertquery="UPDATE Cart SET Quantity={$qty} where CartId=".$id;

                        //echo $query;
                        if(mysqli_query($connection, $Insertquery))
                        {
                            $response=array(
                                'status' => 200,
                                'status_message' =>'Cart Updated Successfully In Cart.'
                            );
                        }
                        else
                        {
                            $response=array(
                                'status' => 500,
                                'status_message' =>'Cart Updation Failed.'
                            );
                        }
                    }

                }
        }
        else
        {

            $Insertquery="INSERT INTO Cart SET ProductId={$ProductId}, Quantity={$Quantity}, UserId={$UserId}";

            //echo $query;
            if(mysqli_query($connection, $Insertquery))
            {
                $response=array(
                    'status' => 200,
                    'status_message' =>'Product Added Successfully In Cart.'
                );
            }
            else
            {
                $response=array(
                    'status' => 500,
                    'status_message' =>'cart Addition Failed.'
                );
            }

        }
        
        header('Content-Type: application/json');
        echo json_encode($response);
    }




   


    function delete_cart($cart_id = 0)
    {
         global $connection;


        $flag=0;
       
            $query1 ="SELECT * from Cart where CartId = {$cart_id}  Limit 1";
            $result=mysqli_query($connection, $query1);
            if($result->num_rows)
            {
                $query="DELETE FROM Cart where CartId=$cart_id";
            //echo $query;
            }
            else
            {
                $response=array(
                'status' => 402,
                'status_message' =>'Insert valid id.');
                $flag=1;
            }
        

        //echo $query;die;
        if( $flag == 0)
        {


            if(mysqli_query($connection, $query))
            {
                $response=array(
                    'status' => 200,
                    'status_message' =>'Product Deleted Successfully.'
                );
            }
            else
            {
                $response=array(
                    'status' => 500,
                    'status_message' =>'Product Deletion Failed.'
                );
            }
        }
        header('Content-Type: application/json');
        echo json_encode($response);
    }


    function delete_cart_by_user_id($user_id)
    {
        global $connection;


        $flag=0;
       
            $query1 ="SELECT * from Cart where UserId = {$user_id}  ";
            $result=mysqli_query($connection, $query1);
            if($result->num_rows)
            {
                $query="DELETE FROM Cart where UserId=$user_id";
            //echo $query;
            }
            else
            {
                $response=array(
                'status' => 402,
                'status_message' =>'id is not in cart.');
                $flag=1;
            }
        

        //echo $query;die;
        if( $flag == 0)
        {


            if(mysqli_query($connection, $query))
            {
                $response=array(
                    'status' => 200,
                    'status_message' =>'Products Deleted Successfully.'
                );
            }
            else
            {
                $response=array(
                    'status' => 500,
                    'status_message' =>'Products Deletion Failed.'
                );
            }
        }
        header('Content-Type: application/json');
        echo json_encode($response);
    }


    



    ?>