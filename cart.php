<?php session_start();   
    if (!isset($_SESSION['login']))
    { 
    	   header('Location:signIn.php');
	   }
    require 'headers.php';
    $user_id = $_SESSION['id'];
    
 ?>

<!DOCTYPE html>
<html lang="en">
    <div class="hero-wrap hero-bread" style="background-image: url('images/banner/bg_1.jpg');">
      <div class="container">
        <div class="row no-gutters slider-text align-items-center justify-content-center">
          <div class="col-md-9 ftco-animate text-center">
          <b><p class="breadcrumbs"><span class="mr-2"><a href="index.html">Home</a></span> <span>Cart</span></p></b>
            <h1 class="mb-0 bread">My Cart</h1>
          </div>
        </div>
      </div>
    </div>

    <section class="ftco-section ftco-cart">
			<div class="container">
				<div class="row">

    			<div class="col-md-12 ftco-animate">
    				<div class="cart-list">
	    				<table class="table">
						    <thead class="thead-primary">
						      <tr class="text-center">
						        <th>&nbsp;</th>
						        <th>&nbsp;</th>
						        <th>Product name</th>
						        <th>Price</th>
						        <th>Quantity</th>
						        <th>Total</th>
						      </tr>
						    </thead>
						    <?php

                 include 'buy/db.php';

                $queryfirst = "SELECT * FROM product,Cart WHERE ProductId=id and UserId = $user_id ";
                $resultfirst = $connection->query($queryfirst);
               /* echo $queryfirst;
                echo isset($resultfirst->num_rows);
                print_r($resultfirst->num_rows);
                //die;*/
                if (isset($resultfirst->num_rows)) {
                  // output data of each row
                	$total=0;
                  while($rowfirst = $resultfirst->fetch_assoc()) {

                        $id_best = $rowfirst['id'];
                        $CartId = $rowfirst['CartId'];
                        $name_best = $rowfirst['name'];
                        $sellingPrice = $rowfirst['sellPrice'];
                        $Description = $rowfirst['description'];
                        $thumbnail_best = $rowfirst['thumbnail'];
                         $quantity = $rowfirst['Quantity'];
                         $Subtotal = $sellingPrice* $quantity;
                         $total = $total + $Subtotal;
           
           

            ?>
						    <tbody>

						      <tr class="text-center">
						        <td class="product-remove"><a href="remove.php/?id=<?=  $CartId ?>"><span class="ion-ios-close"></span></a></td>
						        
						        <td class="image-prod"><div class="img" style="background-image:url(images/products/<?= $thumbnail_best ?>);"></div></td>
						        
						        <td class="product-name">
						        	<h3><?= $name_best ?></h3>
						        </td>
						        
						        <td class="price">₹ <?= $sellingPrice; ?></td>
						        
						        <td class="quantity"><?= $quantity; ?></td>
						        
						        <td class="total"><?= $Subtotal  ?></td>
						      </tr>

						      
						    </tbody>
						    <?php }} ?>
						  </table>
					  </div>
    			</div>
    			 
    		</div>
    		<div class="row justify-content-end">
    			
    			<div class="col-lg-4 mt-5 cart-wrap ftco-animate">
    				<div class="cart-total mb-3">
    					<h3>Cart Totals</h3>
    					<p class="d-flex">
    						<span>Subtotal</span>
    						<span>₹ <?php 
                if (isset($total))
                {
                  echo $total;
                }
                else
                {
                  echo '0.00';
                } 
                ?>
                  
                </span>
    					</p>
    				
    					<hr>
    					<p class="d-flex total-price">
    						<span>Total</span>
    						<span>₹ <?php 
                if (isset($total))
                {
                  echo $total;
                }
                else
                {
                  echo  '0.00';
                } 
                ?></span>
    					</p>
    				</div>
    				<p><a href="checkout.php" class="btn btn-primary py-3 px-4">Proceed to Checkout</a></p>
    			</div>
    		</div>
			</div>
		</section>

		<?php

		 
		  require 'footer.php'; ?>
		}
    
  </body>
</html>