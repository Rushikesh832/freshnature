<?php session_start(); 
if (!isset($_GET['id'])) {
    header('Location:index.php');
  }

if (!isset($_SESSION['login']))
    { 
         header('Location:signIn.php');
    }
  require 'headers.php'; 

  $id_product =$_GET['id'];
?>
  
  <!DOCTYPE html>
<html lang="en">
    <div class="hero-wrap hero-bread" style="background-image: url('images/banner/bg_1.jpg');">
      <div class="container">
        <div class="row no-gutters slider-text align-items-center justify-content-center">
          <div class="col-md-9 ftco-animate text-center">
          	<p class="breadcrumbs"><span class="mr-2"><a href="index.html">Home</a></span> <span class="mr-2"><a href="index.html">Product</a></span> <span>Product Single</span></p>
            <h1 class="mb-0 bread">Product Single</h1>
          </div>
        </div>
      </div>
    </div>
    


    <section class="ftco-section">
    	<div class="container">
    		<div class="row">
          <?php

                 include 'buy/db.php';

                $queryfirst = "SELECT * FROM product WHERE id = $id_product";
                $resultfirst = $connection->query($queryfirst);
                if ($resultfirst->num_rows > 0) {
                  // output data of each row
                  while($rowfirst = $resultfirst->fetch_assoc()) {

                        $id_best = $rowfirst['id'];
                        $name_best = $rowfirst['name'];
                        $price_best = $rowfirst['price'];
                        $sellingPrice = $rowfirst['sellPrice'];
                        $status = $rowfirst['status'];
                        $Description = $rowfirst['description'];
                        $thumbnail_best = $rowfirst['thumbnail'];
           

            ?>
    			<div class="col-lg-6 mb-5 ftco-animate">
    				<a href="images/products/<?= $thumbnail_best; ?>" class="image-popup"><img src="images/products/<?= $thumbnail_best; ?>" class="img-fluid"></a>
    			</div>
    			<div class="col-lg-6 product-details pl-md-5 ftco-animate">
    				<h3><?= $name_best; ?></h3>
    				<div class="d-flex">
                  <div class="pricing">
                    </span><span class="price-sale"><h2>₹ <?= $sellingPrice; ?></h2></span></p>
                  </div>
                </div>
    				<p>
              <?= $Description; ?>
						</p>
						<div class="row mt-4">
							<div class="w-100"></div>
            <form action="addProduct.php?id=<?= $id_product;?>" method="post"  enctype="multipart/form-data">
              <div class="row align-items-end">
                <div class="input-group col-md-6 d-flex mb-3">
                <span class="input-group-btn mr-2">
                    <button type="button" class="quantity-left-minus btn"  data-type="minus" data-field="">
                     <i class="ion-ios-remove"></i>
                    </button>
                  </span>
                <input type="text" id="quantity" name="quantity" class="form-control input-number" value="1" min="1" max="100">
                <span class="input-group-btn ml-2">
                    <button type="button" class="quantity-right-plus btn" data-type="plus" data-field="">
                       <i class="ion-ios-add"></i>
                   </button>
                </span>

              </div>
                <div class="col-md-12">
                   <disv class="form-group mt-4"> 
                  <button type="submit" style="width: 120px; height: 38px; color: green; ">Add Cart </button>

                </div>
              </div>
            </form>
              

                      


	          	<div class="w-100"></div>
          	</div>
    			</div>
          <?php }} ?>
    		</div>
    	</div>
    </section>

    <section class="ftco-section">
    	<div class="container">
				<div class="row justify-content-center mb-3 pb-3">
          <div class="col-md-12 heading-section text-center ftco-animate">
          	<span class="subheading">Products</span>
            <h2 class="mb-4">Related Products</h2>
            <p></p>
          </div>
        </div>   		
    	</div>
    	
    			<div class="container">
        <div class="row">
           <?php

                 include 'buy/db.php';

                $queryfirst = "SELECT * FROM product LIMIT 4";
                $resultfirst = $connection->query($queryfirst);
                if ($resultfirst->num_rows > 0) {
                  // output data of each row
                  while($rowfirst = $resultfirst->fetch_assoc()) {

                        $id_best = $rowfirst['id'];
                        $name_best = $rowfirst['name'];
                        $price_best = $rowfirst['price'];
                        $sellingPrice = $rowfirst['sellPrice'];
                        $status = $rowfirst['status'];
                        $Description = $rowfirst['description'];
                        $thumbnail_best = $rowfirst['thumbnail'];
           

            ?>
          <div class="col-md-6 col-lg-3 ftco-animate">
            <div class="product">
              <a href="images/products/<?= $thumbnail_best; ?>" class="img-prod"><img class="img-fluid" src="images/products/<?= $thumbnail_best; ?>"
                <span class="status"><?= $status; ?></span>
                <div class="overlay"></div>
              </a>
              <div class="text py-3 pb-4 px-3 text-center">
                <h3><a href="#"><?= $name_best; ?></a></h3>
                <div class="d-flex">
                  <div class="pricing">
                    <p class="price"><span class="mr-2 price-dc">₹ <?= $price_best; ?></span><span class="price-sale">₹ <?= $sellingPrice; ?></span></p>
                  </div>
                </div>
                <div class="bottom-area d-flex px-3">
                  <div class="m-auto d-flex">
                    
                    <a href="product-single.php?id=<?= $id_best; ?>" class="buy-now d-flex justify-content-center align-items-center mx-1">
                      <span><i class="ion-ios-cart"></i></span>
                    </a>
                    
                    </a>
                  </div>
                </div>
              </div>
            </div>
          </div>
           <?php }} ?>
        </div>
      </div>
    </section>

    <?php  require 'footer.php'; ?>


  <script>
		$(document).ready(function(){

		var quantitiy=0;
		   $('.quantity-right-plus').click(function(e){
		        
		        // Stop acting like a button
		        e.preventDefault();
		        // Get the field name
		        var quantity = parseInt($('#quantity').val());
            /*document.write(quantity);*/
            //alert(quantity);
		        // If is not undefined
		            
		            $('#quantity').val(quantity + 1);

		          
		            // Increment
		        
		    });


		     $('.quantity-left-minus').click(function(e){
		        // Stop acting like a button
		        e.preventDefault();
		        // Get the field name
		        var quantity = parseInt($('#quantity').val());

		        
		        // If is not undefined
		      
		            // Increment
		            if(quantity>0){
		            $('#quantity').val(quantity - 1);
		            }
		    });
		    
		});

      </script>

    
  </body>
</html>