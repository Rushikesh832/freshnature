-- phpMyAdmin SQL Dump
-- version 5.0.1
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Mar 17, 2020 at 06:00 PM
-- Server version: 10.4.11-MariaDB
-- PHP Version: 7.4.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `TazaFish`
--

-- --------------------------------------------------------

--
-- Table structure for table `banner`
--

CREATE TABLE `banner` (
  `id` int(10) NOT NULL,
  `imagePath` varchar(500) NOT NULL,
  `Description` varchar(500) NOT NULL,
  `name` varchar(500) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `banner`
--

INSERT INTO `banner` (`id`, `imagePath`, `Description`, `name`) VALUES
(7, 'bg_2.jpg', '2nd Banner', 'Food');

-- --------------------------------------------------------

--
-- Table structure for table `Cart`
--

CREATE TABLE `Cart` (
  `CartId` int(11) NOT NULL,
  `ProductId` int(11) NOT NULL,
  `Quantity` int(11) DEFAULT NULL,
  `UserId` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `category`
--

CREATE TABLE `category` (
  `Id` int(11) NOT NULL,
  `CustomCategory` int(20) DEFAULT NULL,
  `name` varchar(255) NOT NULL,
  `Description` varchar(500) NOT NULL,
  `categoryImage` varchar(1000) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `category`
--

INSERT INTO `category` (`Id`, `CustomCategory`, `name`, `Description`, `categoryImage`) VALUES
(1, 22, 'Vegetables', 'Fresh Vegetables', 'category-3.jpg'),
(2, 11, 'Fruits', 'Fruits', 'category-1.jpg'),
(3, 33, 'Juices', 'Juices', 'category-2.jpg'),
(4, 44, 'Dried', 'Dried', 'category-4.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `forgot_password`
--

CREATE TABLE `forgot_password` (
  `Id` int(11) NOT NULL,
  `Userid` int(11) NOT NULL,
  `verificationCode` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `LocalityTable`
--

CREATE TABLE `LocalityTable` (
  `Id` int(11) NOT NULL,
  `locality` varchar(50) NOT NULL,
  `DeliveryCharges` float NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `LocalityTable`
--

INSERT INTO `LocalityTable` (`Id`, `locality`, `DeliveryCharges`) VALUES
(1, 'Satpur', 20),
(2, 'Shramik Nagar', 25),
(3, 'College Road', 30),
(7, 'Sidko', 35);

-- --------------------------------------------------------

--
-- Table structure for table `OrderDetails`
--

CREATE TABLE `OrderDetails` (
  `Id` int(11) NOT NULL,
  `ProductId` int(11) NOT NULL,
  `Quantity` int(11) NOT NULL,
  `SubTotal` int(11) NOT NULL,
  `OrderId` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `OrderDetails`
--

INSERT INTO `OrderDetails` (`Id`, `ProductId`, `Quantity`, `SubTotal`, `OrderId`) VALUES
(1, 46, 1, 99, 1),
(2, 45, 1, 80, 1),
(3, 45, 1, 80, 2),
(4, 45, 1, 80, 3),
(5, 16, 1, 60, 3),
(6, 46, 1, 30, 4),
(7, 45, 1, 80, 4),
(8, 45, 1, 30, 42),
(9, 46, 4, 320, 42),
(10, 46, 5, 495, 43),
(11, 45, 1, 80, 7),
(12, 45, 1, 80, 7),
(13, 46, 1, 99, 8),
(14, 45, 1, 80, 9),
(15, 45, 1, 80, 10),
(16, 45, 1, 80, 11),
(17, 17, 1, 30, 12),
(18, 45, 1, 80, 13),
(19, 45, 1, 80, 14),
(24, 46, 11, 1089, 34),
(25, 45, 1, 80, 34),
(26, 45, 3, 240, 35),
(27, 45, 5, 400, 36),
(28, 45, 3, 240, 37),
(29, 20, 2, 300, 38),
(30, 45, 4, 320, 38),
(31, 32, 2, 134, 38),
(32, 45, 5, 400, 39),
(33, 32, 2, 134, 40),
(34, 46, 3, 297, 41),
(35, 46, 1, 99, 42),
(36, 45, 1, 100, 43),
(37, 49, 1, 400, 43),
(38, 45, 5, 500, 44),
(39, 45, 11, 1100, 45),
(40, 46, 1, 800, 45),
(41, 50, 1, 900, 45),
(42, 45, 1, 100, 46),
(43, 45, 2, 200, 47),
(44, 45, 2, 200, 48),
(45, 46, 1, 800, 49),
(46, 45, 1, 100, 51),
(47, 45, 1, 100, 52),
(48, 48, 6, 4800, 53),
(49, 50, 1, 900, 54),
(50, 49, 2, 800, 56),
(51, 45, 2, 200, 57),
(52, 45, 2, 200, 58),
(53, 46, 2, 1600, 60),
(54, 45, 2, 200, 61),
(55, 46, 2, 1600, 61),
(56, 46, 2, 1600, 62),
(57, 45, 3, 300, 62),
(58, 45, 2, 200, 63),
(59, 46, 2, 1600, 63);

-- --------------------------------------------------------

--
-- Table structure for table `Orders`
--

CREATE TABLE `Orders` (
  `OrderId` int(11) NOT NULL,
  `UserId` int(11) NOT NULL,
  `Total` int(11) DEFAULT NULL,
  `Quantity` int(11) DEFAULT NULL,
  `DeliveryAddress` varchar(500) DEFAULT NULL,
  `City` varchar(50) DEFAULT NULL,
  `Locality` varchar(50) DEFAULT NULL,
  `PaymentMethod` varchar(100) DEFAULT NULL,
  `Status` varchar(100) DEFAULT NULL,
  `RazorpayId` varchar(100) DEFAULT NULL,
  `OrderPlacedTime` timestamp NOT NULL DEFAULT current_timestamp(),
  `DeliveryDate` varchar(45) DEFAULT NULL,
  `DeliverySlot` varchar(45) DEFAULT NULL,
  `DeliveryCharges` float NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `Orders`
--

INSERT INTO `Orders` (`OrderId`, `UserId`, `Total`, `Quantity`, `DeliveryAddress`, `City`, `Locality`, `PaymentMethod`, `Status`, `RazorpayId`, `OrderPlacedTime`, `DeliveryDate`, `DeliverySlot`, `DeliveryCharges`) VALUES
(4, 12, 110, 2, '5,gangai heights', '', '', 'Cash', 'complete', NULL, '2020-01-14 09:15:48', '12-10-2020', '6-12 pm', 0),
(5, 11, 30, 1, '1323,MHB colony', '', '', 'online', 'paid', 'pay_E4ItbqZUs2SpHZ', '2020-01-14 09:18:53', NULL, NULL, 0),
(6, 11, 815, 9, '16,gangai heights,Panhala boys hostle,ambegoan-41', '', '', 'online', 'paid', 'pay_E6H01t3PLGLlPI', '2020-01-19 08:45:27', NULL, NULL, 0),
(7, 11, 160, 2, '', '', '', 'online', 'paid', 'pay_EADzh2x9SIEvXO', '2020-01-29 08:25:02', NULL, NULL, 0),
(8, 11, 99, 1, '', '', '', 'Cash', 'paid', NULL, '2020-01-29 08:36:02', NULL, NULL, 0),
(9, 11, 80, 1, '1323,MHB colony', '', '', 'Cash', 'progress', NULL, '2020-01-29 08:39:00', NULL, NULL, 0),
(10, 11, 80, 1, '1323,MHB colony', '', '', 'Cash', 'paid', NULL, '2020-01-29 08:45:04', NULL, NULL, 0),
(11, 11, 80, 1, '16,gangai heights,Panhala boys hostle,ambegoan-41', '', '', 'Cash', 'paid', NULL, '2020-01-29 08:48:44', NULL, NULL, 0),
(12, 11, 30, 1, '16,gangai heights,Panhala boys hostle,ambegoan-41', '', '', 'Cash', 'paid', NULL, '2020-01-29 08:53:28', NULL, NULL, 0),
(13, 11, 80, 1, '1323,MHB colony', '', '', 'Cash', 'paid', NULL, '2020-01-29 08:59:11', NULL, NULL, 0),
(14, 11, 80, 1, '1323,MHB colony', '', '', 'Cash', 'paid', NULL, '2020-01-29 09:00:40', NULL, NULL, 0),
(34, 12, 1169, 12, 'gjhvgh', 'nashik', 'colony', 'cash', 'progress', NULL, '2020-02-29 11:20:10', NULL, NULL, 0),
(35, 11, 240, 3, 'Sant Narahari Maharaj Rd', 'Nashik', 'Vashi', 'cash', 'new', NULL, '2020-03-01 07:24:33', NULL, NULL, 0),
(36, 11, 400, 5, 'Sant Narahari Maharaj Rd', 'Nashik', 'Vashi', 'cash', 'complete', NULL, '2020-03-01 08:55:44', NULL, NULL, 0),
(37, 11, 240, 3, 'PUNE CITY', 'BIHAR', 'Vashi', 'cash', '1', NULL, '2020-03-01 08:56:28', NULL, NULL, 0),
(38, 11, 754, 8, 'Sant Narahari Maharaj Rd', 'Nashik', 'Vashi', 'cash', '1', NULL, '2020-03-01 10:57:20', NULL, NULL, 0),
(39, 11, 400, 5, 'bshshs', 'djhdh', 'Vashi', 'cash', '1', NULL, '2020-03-01 11:04:20', NULL, NULL, 0),
(40, 11, 134, 2, 'Sant Narahari Maharaj Rd', 'Nashik', 'Kalamboli', 'cash', '1', NULL, '2020-03-01 16:25:45', NULL, NULL, 0),
(41, 11, 297, 3, 'hshsh', 'hdhdhd', 'Vashi', 'cash', '1', NULL, '2020-03-01 17:20:09', NULL, NULL, 0),
(42, 38, 990, 1, 'nssjjd', 'rjjd', 'Vashi', 'cash', '1', NULL, '2020-03-01 18:15:53', NULL, NULL, 0),
(43, 38, 550, 2, 'hheheudurhhR', 'hrjrjr', 'Vashi', 'cash', '1', NULL, '2020-03-02 11:36:49', NULL, NULL, 0),
(44, 54, 500, 5, 'Kumharpara N\nNear Kali Mandir Dumka ', 'gyh', 'Vashi', 'cash', '1', NULL, '2020-03-02 11:42:54', NULL, NULL, 0),
(45, 38, 2800, 13, 'ggg', 'gg', 'Vashi', 'cash', '1', NULL, '2020-03-03 17:34:40', NULL, NULL, 0),
(46, 12, 100, 1, 'gjhvgh', 'nashik', 'colony', 'cash', '1', NULL, '2020-03-09 08:12:40', '6-12 pm', '12-10-2020', 0),
(47, 12, 200, 2, 'gjhvgh', 'nashik', 'colony', 'cash', 'new', NULL, '2020-03-09 08:32:50', '6-12 pm', '12-10-2020', 0),
(48, 12, 200, 2, 'gjhvgh', 'nashik', 'colony', 'cash', 'new', NULL, '2020-03-09 17:52:32', '6-12 pm', '12-10-2020', 20),
(49, 12, 800, 1, 'gjhvgh', 'nashik', 'colony', 'cash', 'new', NULL, '2020-03-09 17:53:26', '6-12 pm', '12-10-2020', 20),
(50, 12, NULL, NULL, 'gjhvgh', 'nashik', 'colony', 'cash', 'new', NULL, '2020-03-09 17:54:20', '6-12\r\npm', '12-10-2020', 20),
(51, 12, 120, 1, 'gjhvgh', 'nashik', 'colony', 'cash', 'new', NULL, '2020-03-11 19:31:06', '12-10-2020', '6-12 pm', 20),
(52, 39, 120, 1, 'gjhvgh', 'nashik', 'colony', 'cash', 'new', NULL, '2020-03-14 17:29:55', '12-10-2020', '6-12 pm', 20),
(53, 39, 4820, 6, 'gjhvgh', 'nashik', 'colony', 'cash', 'new', NULL, '2020-03-14 17:31:55', '12-10-2020', '6-12 pm', 20),
(54, 38, 920, 1, 'gjhvgh', 'nashik', 'colony', 'cash', 'new', NULL, '2020-03-15 17:55:27', '12-10-2020', '6-12 pm', 20),
(55, 38, NULL, NULL, 'gjhvgh', 'nashik', 'colony', 'online', 'new', 'GH5578HJ', '2020-03-15 18:13:47', '12-10-2020', '6-12 pm', 20),
(56, 38, 820, 2, 'gjhvgh', 'nashik', 'colony', 'online', 'new', 'GH5578HJ', '2020-03-15 18:14:02', '12-10-2020', '6-12 pm', 20),
(57, 38, 220, 2, 'gjhvgh', 'nashik', 'colony', 'cash', 'new', '', '2020-03-15 18:16:21', '12-10-2020', '6-12 pm', 20),
(58, 36, 220, 2, 'gjhvgh', 'nashik', 'colony', 'cash', 'new', '', '2020-03-15 18:17:08', '12-10-2020', '6-12 pm', 20),
(59, 42, NULL, NULL, 'gjhvgh', 'nashik', 'colony', 'cash', 'new', '', '2020-03-15 01:33:49', '12-10-2020', '6-12 pm', 20),
(60, 52, 1620, 2, 'gjhvgh', 'nashik', 'colony', 'cash', 'new', '', '2020-03-15 01:33:38', '12-10-2020', '6-12 pm', 20),
(61, 39, 1820, 4, 'gjhvgh', 'nashik', 'colony', 'cash', 'new', '', '2020-03-15 01:33:08', '12-10-2020', '6-12 pm', 20),
(62, 42, 1920, 5, 'gjhvgh', 'nashik', 'colony', 'cash', 'new', '', '2020-03-15 01:33:59', '12-10-2020', '6-12 pm', 20),
(63, 42, 1820, 4, 'gjhvgh', 'nashik', 'colony', 'cash', 'new', '', '2020-03-16 07:04:26', '12-10-2020', '6-12 pm', 20);

-- --------------------------------------------------------

--
-- Table structure for table `product`
--

CREATE TABLE `product` (
  `id` int(11) NOT NULL,
  `name` varchar(1000) NOT NULL,
  `description` varchar(1000) NOT NULL,
  `category` int(11) NOT NULL,
  `price` float NOT NULL,
  `thumbnail` varchar(1000) NOT NULL,
  `gross` varchar(45) DEFAULT NULL,
  `netwt` varchar(45) DEFAULT NULL,
  `sellPrice` float NOT NULL,
  `status` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `product`
--

INSERT INTO `product` (`id`, `name`, `description`, `category`, `price`, `thumbnail`, `gross`, `netwt`, `sellPrice`, `status`) VALUES
(45, 'Fruits', 'Gorss wt 500 and Net wight 500 very dg ghgds sgs dfusdf iusfisugfsuidf siufisufisu usiufis', 11, 500, 'unnamed.jpg', '10', '2', 100, '1'),
(46, 'Fisha Royal | Gross Wt 900 Net wt 800', 'Fresh Fish is best source of ministerial and vitrine', 11, 900, 'f4.jpg', NULL, NULL, 800, '20'),
(47, 'Fish Maya | Gross wt 500 Net Wt 400', 'Fish is filled with omega-3 fatty acids and vitamins such as D and B2 &#40;riboflavin&#41;. Fish is rich in calcium and phosphorus and a great source of minerals, such as iron, zinc, iodine, magnesium, and potassium. The American Heart Association recommends eating fish at least two times per week as part of a healthy diet.', 22, 800, 'FG.jpg', NULL, NULL, 700, '20'),
(48, 'Fisha USA | Gross Wt 900 Net wt 800', 'Fish is also a great source of omega-3 fatty acids, which are incredibly important for your body and brain.', 22, 900, 'fishss.jpg', '1Kg', '1 Kg', 800, '20'),
(49, 'Fish Rehu | Gross wt 500 Net Wt 400', 'Fatty species are sometimes considered the healthiest. That&#x27;s because fatty fish, including salmon, trout, sardines, tuna, and mackerel, are higher in fat-based nutrients', 44, 500, 'f3.jpg', NULL, NULL, 400, '10'),
(50, 'Fish Bangla | Gross wt 500 Net Wt 400', 'To meet your omega-3 requirements, eating fatty fish at least once or twice a week is recommended. If you are a vegan, opt for omega-3 supplements made from microalgae.', 33, 1000, 'kingfish-sashimi_2.jpg', NULL, NULL, 900, '20');

-- --------------------------------------------------------

--
-- Table structure for table `slots`
--

CREATE TABLE `slots` (
  `Id` int(50) NOT NULL,
  `Slot` varchar(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `slots`
--

INSERT INTO `slots` (`Id`, `Slot`) VALUES
(1, '7 AM - 9 AM'),
(2, '9 AM - 11 AM'),
(3, '11 AM - 1 PM'),
(4, '1 PM - 3 AM'),
(5, '3 PM - 5 PM'),
(6, '5 PM - 7 PM'),
(7, '7 PM - 9 PM');

-- --------------------------------------------------------

--
-- Table structure for table `userAddress`
--

CREATE TABLE `userAddress` (
  `Id` int(11) NOT NULL,
  `Address1` varchar(500) DEFAULT NULL,
  `Type1` varchar(100) DEFAULT NULL,
  `Address2` varchar(500) DEFAULT NULL,
  `Type2` varchar(100) DEFAULT NULL,
  `Address3` varchar(500) DEFAULT NULL,
  `Type3` varchar(100) DEFAULT NULL,
  `NoOfAddresses` int(11) NOT NULL DEFAULT 0,
  `DefaultAddress` varchar(500) NOT NULL,
  `UserId` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `userAddress`
--

INSERT INTO `userAddress` (`Id`, `Address1`, `Type1`, `Address2`, `Type2`, `Address3`, `Type3`, `NoOfAddresses`, `DefaultAddress`, `UserId`) VALUES
(6, '5,gangai heights', 'work', '1323,MHB colony', 'home', '16,gangai heights,Panhala boys hostle,ambegoan-41', 'home', 3, '0', 11),
(7, 'jvb', 'home', 'jhmnhkjl;km;', 'work', NULL, NULL, 2, '1', 10),
(8, 'vadgaon bk', 'home', 'pune', 'other', NULL, NULL, 2, '1', 14);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `Id` int(11) NOT NULL,
  `email` varchar(255) NOT NULL,
  `phoneNumber` varchar(20) DEFAULT NULL,
  `firstname` varchar(255) NOT NULL,
  `lastname` varchar(255) NOT NULL,
  `password` varchar(500) NOT NULL,
  `address` varchar(1000) NOT NULL,
  `addressType` varchar(100) DEFAULT NULL,
  `city` varchar(100) NOT NULL,
  `country` varchar(100) NOT NULL,
  `Locality` varchar(50) NOT NULL,
  `role` varchar(100) NOT NULL,
  `IsEmailVerify` tinyint(1) DEFAULT 0,
  `VerificationCode` varchar(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`Id`, `email`, `phoneNumber`, `firstname`, `lastname`, `password`, `address`, `addressType`, `city`, `country`, `Locality`, `role`, `IsEmailVerify`, `VerificationCode`) VALUES
(11, 'rushikeshvispute832@gmail.com', NULL, 'Rushikesh', 'Vispute', '7c4a8d09ca3762af61e59520943dc26494f8941b', '16,gangai heights,Panhal boys hostle,ambegoan-041', '', 'Pune', 'India', '', 'admin', 1, '6a71hv'),
(12, 'rushikeshvispute55@gmail.com', NULL, 'Mayuresh', 'Satao', '93b1a1c7485eb79a3bd241db795a945ffb15d663', '407, A, Asawari, Nanded City, Pune', 'home', 'Pune', 'India', '', 'admin', 0, '0'),
(13, 'msatao78@gmail.com', NULL, 'Mayuresh', 'Satao', '601f1889667efaebb33b8c12572835da3f027f78', '407, A, Asawari, Nanded City, Pune', 'home', 'Pune', 'India', '', 'client', 0, '0'),
(14, 'svshirse@gmail.com', NULL, 'Shubham', 'Shirse', 'e5c92629b931d368327b0548dee36c511cd12253', 'Shivaji Street', 'home', 'Ahamadnagar', 'India', '', 'client', 0, '0'),
(24, 'sumitgharate@gmail.com', NULL, 'Kunal', 'Gharate', '7c4a8d09ca3762af61e59520943dc26494f8941b', 'Sant Narahari Maharaj Rd', 'home', 'Karave Nagar', 'india', 'MUMBAI', 'client', 1, '111111'),
(29, 'kunalgharate@gmail.com', NULL, 'Kunal', 'Gharate', '7c4a8d09ca3762af61e59520943dc26494f8941b', 'Sant Narahari Maharaj Rd', 'home', 'Karave Nagar', 'india', 'MUMBAI', 'client', 1, '111111'),
(35, 'grossifyservices@gmail.com', NULL, 'paresh', 'more', '7c4a8d09ca3762af61e59520943dc26494f8941b', 'Sant Narahari Maharaj Rd', 'home', 'Kamothe', 'india', 'Nashik', 'client', 1, '111111'),
(36, 'krishna@gmail.com', NULL, 'krishna', 'kumar', '2ed45186c72f9319dc64338cdf16ab76b44cf3d1', 'newhgsggs', 'home', 'nerul', 'india', 'mum', 'client', 1, '111111'),
(38, 'kk@gmail.com', NULL, 'kk', 'kk', '2ed45186c72f9319dc64338cdf16ab76b44cf3d1', 'jdjjd', 'home', 'Karave Nagar', 'india', 'dumja', 'client', 1, '111111'),
(39, 'Raghuvirpratap123@gmail.com ', NULL, 'Raghuvir', 'Pratap', '6506d0c3378515e5d5ce4852db6de347b4b3969c', 'Mumbai ', 'home', 'Vashi', 'india', 'Mumbai', 'client', 1, '111111'),
(42, 'sonuravi4488@gmail.com', NULL, 'Sonu', 'Kumar', '38554dfe3d5d5275f9276e2791cc3a637dd1b4bb', 'manpur sudhi tola', 'home', 'Vashi', 'india', 'mumbai\n', 'client', 1, '111111'),
(52, 'jp@gmail.com', NULL, 'ramesh', 'kumar', '2ed45186c72f9319dc64338cdf16ab76b44cf3d1', 'nndn', 'home', 'Kamothe', 'india', 'delhi', 'client', 1, '111111'),
(53, 'jay.uninor@gmail.com ', NULL, 'Jay ', 'Prakash ', 'd4649863b691bbe5371619bace195914e002db24', 'Dumka ', 'home', 'Vashi', 'india', 'Dumka ', 'client', 1, '111111'),
(54, 'jay@gmail.com ', NULL, 'Jay ', 'Prakash ', '2ed45186c72f9319dc64338cdf16ab76b44cf3d1', 'Dumka ', 'home', 'Vashi', 'india', 'Dumka ', 'client', 1, '111111');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `banner`
--
ALTER TABLE `banner`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `Cart`
--
ALTER TABLE `Cart`
  ADD PRIMARY KEY (`CartId`),
  ADD UNIQUE KEY `ProductId` (`ProductId`,`UserId`);

--
-- Indexes for table `category`
--
ALTER TABLE `category`
  ADD PRIMARY KEY (`Id`),
  ADD UNIQUE KEY `CustomCategory` (`CustomCategory`);

--
-- Indexes for table `forgot_password`
--
ALTER TABLE `forgot_password`
  ADD PRIMARY KEY (`Id`);

--
-- Indexes for table `LocalityTable`
--
ALTER TABLE `LocalityTable`
  ADD PRIMARY KEY (`Id`);

--
-- Indexes for table `OrderDetails`
--
ALTER TABLE `OrderDetails`
  ADD PRIMARY KEY (`Id`);

--
-- Indexes for table `Orders`
--
ALTER TABLE `Orders`
  ADD PRIMARY KEY (`OrderId`);

--
-- Indexes for table `product`
--
ALTER TABLE `product`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `slots`
--
ALTER TABLE `slots`
  ADD KEY `Id` (`Id`);

--
-- Indexes for table `userAddress`
--
ALTER TABLE `userAddress`
  ADD PRIMARY KEY (`Id`),
  ADD UNIQUE KEY `UserId` (`UserId`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`Id`),
  ADD UNIQUE KEY `email` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `banner`
--
ALTER TABLE `banner`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `Cart`
--
ALTER TABLE `Cart`
  MODIFY `CartId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=258;

--
-- AUTO_INCREMENT for table `category`
--
ALTER TABLE `category`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT for table `forgot_password`
--
ALTER TABLE `forgot_password`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `LocalityTable`
--
ALTER TABLE `LocalityTable`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `OrderDetails`
--
ALTER TABLE `OrderDetails`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=60;

--
-- AUTO_INCREMENT for table `Orders`
--
ALTER TABLE `Orders`
  MODIFY `OrderId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=64;

--
-- AUTO_INCREMENT for table `product`
--
ALTER TABLE `product`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=59;

--
-- AUTO_INCREMENT for table `slots`
--
ALTER TABLE `slots`
  MODIFY `Id` int(50) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `userAddress`
--
ALTER TABLE `userAddress`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=55;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
