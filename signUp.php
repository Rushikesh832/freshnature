<?php session_start();  
  require 'headers.php'; 

 if (isset($_SESSION['login'])) {
    
    ?>
    <!DOCTYPE html>
<html lang="en">
<section id="home-section" class="hero">
      <div class="home-slider owl-carousel">
        <div class="slider-item" style="background-image: url(images/banner/bg_1.jpg">
          <div class="overlay"></div>
          <div class="container">
            <div class="row slider-text justify-content-center align-items-center" data-scrollax-parent="true">

              <div class="col-md-12 ftco-animate text-center">
                <h1 class="mb-2">You Have Already Login</h1>
                <h2 class="subheading mb-4">We deliver organic vegetables &amp; fruits</h2>
                <p><a href="index.php" class="btn btn-primary">Go To Home</a></p>
              </div>

            </div>

          </div>

        </div>
      </div>
    </section>
    <?php
  }
  else
  {
?>
  <style type="text/css">
  hr {
  border-style: double;
  border-width: 2px;
}
</style>

  <hr size="2px">

    <section class="ftco-section">
      <div class="container">
        <div class="row justify-content-center">
          <div class="col-xl-7 ftco-animate">
            <form action="signUpData.php" method="post"  enctype="multipart/form-data">
              <h3 class="mb-4 billing-heading">Registration</h3>
              <div class="row align-items-end">
                <div class="col-md-6">
                  <div class="form-group">
                    <label for="firstname">First Name</label>
                    <input type="text" name="firstname" class="form-control" placeholder="" required="">
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="form-group">
                    <label for="lastname">Last Name</label>
                    <input type="text"  name="lastname" class="form-control" placeholder=""  required="">
                  </div>
                </div>
                <div class="w-100"></div>
                <div class="col-md-6">
                  <div class="form-group">
                    <label for="country">Country</label>
                    <input type="text"  name="country" class="form-control" placeholder="" required="">
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="form-group">
                    <label for="email">Email Address</label>
                    <input type="text"  name="email" class="form-control" placeholder="" required="">
                  </div>
                </div>
                <div class="w-100"></div>
                <div class="col-md-6">
                  <div class="form-group">
                    <label for="streetaddress">Street Address</label>
                    <input type="text" class="form-control" name="address" placeholder="House number and street name" required="">
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="form-group">
                    <label for="towncity">Town / City</label>
                    <input type="text" class="form-control" name="city" placeholder="" required="">
                  </div>
                </div>
                <div class="form-group">
                    <div class="col-md-3">
                      <div class="radio">
                         <label><input type="radio" name="addresstype" required="" class="mr-2" value="home"> Home</label>
                      </div>
                    </div>
                  </div>
                  
                  <div class="form-group">
                    <div class="col-md-3">
                      <div class="radio">
                         <label><input type="radio" name="addresstype" required="" class="mr-2" value="work"> Work</label>
                      </div>
                    </div>
                  </div>

                  <div class="form-group">
                    <div class="col-md-3">
                      <div class="radio">
                         <label><input type="radio" name="addresstype" required="" class="mr-2" value="other"> Other</label>
                      </div>
                    </div>
                  </div>
                <div class="w-100"></div>
                <div class="col-md-6">
                  <div class="form-group">
                    <label for="Password">Password</label>
                    <input type="Password" class="form-control" name="Password" placeholder="" required="">
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="form-group">
                    <label for="Confirm Password">Confirm Password</label>
                    <input type="Password" class="form-control"  name="cpassword" placeholder="" required="">
                  </div>
                </div>
                <div class="w-100"></div>
                <div class="col-md-12">
                  <div class="form-group mt-4">
                  <button type="submit" class="btn btn-info"  name ="signUp" style="width: 100px; height: 32px">Sign Up</button>
                  <a href="signIn.php" class="btn btn-success" style="margin-left: 40px;width: 100px; height: 32px;text-align: center;">Sign In</a>
                  </div>
                </div>
              </div>
            </form><!-- END -->
          </div>
        </div>
      </div>
    </section> <!-- .section -->

    <?php  
        }
    require 'footer.php'; ?>
    
  </body>
</html>