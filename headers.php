<!DOCTYPE html>
<html lang="en">
  <head><meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <title>Sabse Sasta Online </title>
    
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    
    <link href="https://fonts.googleapis.com/css?family=Poppins:200,300,400,500,600,700,800&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Lora:400,400i,700,700i&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Amatic+SC:400,700&display=swap" rel="stylesheet">
     <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css"> 

    <link rel="stylesheet" href="css/open-iconic-bootstrap.min.css">
    <link rel="stylesheet" href="css/animate.css">
    
    <link rel="stylesheet" href="css/owl.carousel.min.css">
    <link rel="stylesheet" href="css/owl.theme.default.min.css">
    <link rel="stylesheet" href="css/magnific-popup.css">

    <link rel="stylesheet" href="css/aos.css">

    <link rel="stylesheet" href="css/ionicons.min.css">

    <link rel="stylesheet" href="css/bootstrap-datepicker.css">
    <link rel="stylesheet" href="css/jquery.timepicker.css">

    
    <link rel="stylesheet" href="css/flaticon.css">
    <link rel="stylesheet" href="css/icomoon.css">
    <link rel="stylesheet" href="css/style.css">

  </head>
  <body class="goto-here">
	<div class="py-1 bg-primary">
    	<div class="container">
    		<div class="row no-gutters d-flex align-items-start align-items-center px-md-0">
	    		<div class="col-lg-12 d-block">
		    		<div class="row d-flex">
		    			<div class="col-md pr-4 d-flex topper align-items-center">
					    	<div class="icon mr-2 d-flex justify-content-center align-items-center"><span class="icon-phone2"></span></div>
						    <span class="text">+91 84848 29428</span>
					    </div>
					    <div class="col-md pr-4 d-flex topper align-items-center">
					    	<div class="icon mr-2 d-flex justify-content-center align-items-center"><span class="icon-paper-plane"></span></div>
						    <span class="text">vjenterprises113@gmail.com</span>
					    </div>
					    <div class="col-md-5 pr-4 d-flex topper align-items-center text-lg-right">
						    <span class="text">3-5 Business days delivery &amp; Free Returns</span>
					    </div>
				    </div>
			    </div>
		    </div>
		  </div>
    </div>
    <nav class="navbar navbar-expand-lg navbar-dark ftco_navbar bg-dark ftco-navbar-light" id="ftco-navbar">
	    <div class="container">
	      <a class="navbar-brand" href="index.php">SABSE SASTA ONLINE</a>
	      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#ftco-nav" aria-controls="ftco-nav" aria-expanded="false" aria-label="Toggle navigation">
	        <span class="oi oi-menu"></span> Menu
	      </button>

	      <div class="collapse navbar-collapse" id="ftco-nav">
	        <ul class="navbar-nav ml-auto">
	          <li class="nav-item active"><a href="index.php" class="nav-link">Home</a></li>
	          <li class="nav-item dropdown">
              <a class="nav-link dropdown-toggle" href="" id="dropdown04" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Shop</a>
              <div class="dropdown-menu" aria-labelledby="dropdown04">
              	<a class="dropdown-item" href="shop.php">Products</a>
                <a class="dropdown-item" href="cart.php">Cart</a>
                <a class="dropdown-item" href="checkout.php">Checkout</a>
              </div>
            </li>
<?php if (isset($_SESSION['login'])) 
					   { 
            				$user_id =$_SESSION['id'] ;
            				include 'buy/db.php';

            			$query = "SELECT count(*) as sum FROM Cart WHERE UserId = $user_id";
						$sum = mysqli_query($connection, $query);
						if (!$sum) 
						{
							$CartSum=0;
						}
						else
						{
							$row = mysqli_fetch_array($sum);

							$CartSum = $row['sum'];
							
						}
	              	  
					   	$number=$CartSum;
					   }
?>



				<li class="nav-item dropdown">
              <a class="nav-link dropdown-toggle" href="" id="dropdown04" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Categories</a>
              <div class="dropdown-menu" aria-labelledby="dropdown04">
              	<?php
              	include 'buy/db.php';

              	 $queryfirst = "SELECT * FROM category LIMIT 10";
                $resultfirst = $connection->query($queryfirst);
                if ($resultfirst->num_rows > 0) {
                  // output data of each row
                  while($rowfirst = $resultfirst->fetch_assoc()) {

                        $id_best = $rowfirst['Id'];
                        $name_best = $rowfirst['name'];
						?>
              	<a class="dropdown-item" href="Category.php?id=<?= $id_best?>"><?= $name_best;?></a>
              <?php  }} ?>
              </div>
            </li>
            <li class="nav-item"><a href="Gallary.php" class="nav-link">Gallary</a></li>

	          <li class="nav-item"><a href="about.php" class="nav-link">About</a></li>
	          <li class="nav-item"><a href="contact.php" class="nav-link">Contact</a></li>
	          <li class="nav-item cta cta-colored"><a href="cart.php" class="nav-link"><span class="icon-shopping_cart">[<?php 
	          	if (isset($_SESSION['login'])) 
					   { echo $number;}else {
					   	echo '0';
					   } ?>]</span></a></li>
	          <li class="nav-item dropdown">
	              <a class="nav-link dropdown-toggle" href="#" id="dropdown04" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Profile</a>
	              <div class="dropdown-menu" aria-labelledby="dropdown04">
	              	<?php 
	              	  if (!isset($_SESSION['login'])) 
					   { 
					     
					  ?>
	              	<a class="dropdown-item" href="signIn.php">SignIn</a>
	                <a class="dropdown-item" href="signUp.php">SignUp</a>
	                <?php
	            }
	                if (isset($_SESSION['login'])) 
					   { 
					     
					  ?>
					  	<a class="dropdown-item" href="Profile.php">Profile</a>
	                 	<a class="dropdown-item" href="addAddress.php">Add Address</a>
		                <a class="dropdown-item" href="showOrders.php">Your Orders</a>
		                <a class="dropdown-item" href="logout.php">Logout</a>
	                <?php
	            }
	            ?>
	              </div>
	            </li>


	             <?php

	            if (isset($_SESSION['role']) == 'admin') 
					   { 
?>
	            <li class="nav-item active"><a href="buy/admin" class="nav-link">Admin</a></li>

	        <?php } ?>




	        </ul>
	      </div>
	    </div>
	  </nav>
   </body>
</html>