<?php session_start();  

    require '../headers.php';
    $user_id = $_SESSION['id'];
    $OrderId =$_GET['Id'];

     include '../db.php';

     


 ?>
<!DOCTYPE html>
<html lang="en">

    <div class="hero-wrap hero-bread" style="background-image: url('../../images/banner/bg_1.jpg');">
      <div class="container">
        <div class="row no-gutters slider-text align-items-center justify-content-center">
          <div class="col-md-9 ftco-animate text-center">
            <p class="breadcrumbs"><span class="mr-2"><a href="index.html">Home</a></span> <span>Order Preview</span></p>
            <h1 class="mb-0 bread">Preview</h1>
          </div>
        </div>
      </div>
    </div>

    <section class="ftco-section ftco-cart">
      <div class="container">
        <div class="row">

          <div class="col-md-12 ftco-animate">
            <div class="cart-list">
              <table class="table">
                <thead class="thead-primary">
                  <tr class="text-center">
                    <th>&nbsp;</th>
                    <th>&nbsp;</th>
                    <th>Product Name</th>
                      <th>Market Price</th>
                    <th>Selling Price</th>
                    <th>Gross Wt.</th>
                    <th>Net Wt.</th>
                    <th>Quantity</th>
                    <th>Subtotal</th>

                  </tr>
                </thead>
                <?php

                 include '../db.php';
                 $DeliveryAddress='';

                $queryfirst = "SELECT *,OrderDetails.Quantity as product_Quantity FROM OrderDetails,product,Orders WHERE ProductId  = product.id AND Orders.OrderId=$OrderId  And Orders.OrderId = OrderDetails.OrderId ";
                //echo $queryfirst;die;
                $resultfirst = $connection->query($queryfirst);
               
                if (isset($resultfirst->num_rows)) {
                  $count=0;
                  $total=0;
                  while($rowfirst = $resultfirst->fetch_assoc()) {

                        $name = $rowfirst['name'];
                        $Sprice = $rowfirst['sellPrice'];
                      $price = $rowfirst['price'];
                        $thumbnail = $rowfirst['thumbnail'];
                        $Quantity = $rowfirst['product_Quantity'];
                        $SubTotal = $rowfirst['SubTotal'];
                        $gross = $rowfirst['gross'];
                        $netwt = $rowfirst['netwt'];
                      $DeliveryCharges = $rowfirst['DeliveryCharges'];
                        
                        $total=$total+$SubTotal;
                        
                        $count=$count+1;
                        $DeliveryAddress = $rowfirst['DeliveryAddress'];
                         
           
           

            ?>
                <tbody>

                  <tr class="text-center">
                    <td class="Order-count"><b><?= $count ?></b></td>
                    
                    <td class="image-prod" ><div class="img" style="background-image:url(images/products/<?= $thumbnail ?>);"></div></td>
                    
                    
                    
                    
                    <td class="PaymentMethod"> <b><?= $name; ?></b></td>

                    <td class="Order Placed Time"><b><?= $price; ?></b></td>
                      <td class="Order Placed Time"><b><?= $Sprice; ?></b></td>

                     <td class="gross wt."><b><?= $gross; ?></b></td>

                    <td class="Net Wt."><b><?= $netwt ?></b></td>

                    <td class="Quantity"><b><?= $Quantity ?></b></td>
                    
                   <td class="Subtotal">
                      <h3><b><?= $SubTotal ?></b></h3>
                    </td>
                  </tr>

                  
                </tbody>
                <?php }} ?>
              </table>
            </div>
          </div>

          <div class="col-md-10 d-flex mb-5">
                <div class="cart-detail cart-total p-3 p-md-4">
                  <h3 class="billing-heading mb-4">Order Total</h3>
                  
                  <p class="d-flex">
                    <span>Delivery</span>
                    <span>₹<?php
                        if (isset($DeliveryCharges) && $DeliveryCharges != 0 )
                        {
                            echo $DeliveryCharges.'.00';
                        }
                        else
                        {
                            echo '0.00';
                        }
                        ?></span>
                  </p>
                  <p class="d-flex">
                    <span>Discount</span>
                    <span>₹0.00</span>
                  </p>
                  <hr>
                  <p class="d-flex total-price">
                    <span>Total</span>
                    <span>₹ <?php 
                if (isset($total))
                {
                  echo $total+$DeliveryCharges.'.00';
                }
                else
                {
                  echo '0.00';
                } 
                ?></span>
                  </p>
                </div>
                <h3 class="mb-0 bread" style="color: green "><?php echo "Delivery Address: " .$DeliveryAddress;  ?></h3>
              </div>
           
        </div>
        
      </div>
    </section>

    <?php
      require '../footer.php'; ?>
  </body>
</html>