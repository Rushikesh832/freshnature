<?php session_start(); 

   if (!isset($_SESSION['login'])) 
   { 
     header('Location: signIn.php');
  }
  if ($_SESSION['role'] != 'admin') {
  header('Location: ../../index.php');

}
   require '../headers.php';


         
  
 ?>

   <!DOCTYPE html>
<html lang="en">
   


    <section class="ftco-section ftco-cart">
      <div class="container">
        <div class="row justify-content-center mb-3 pb-3">
          <div class="col-md-12 heading-section text-center ftco-animate">
            <h2 class="mb-4">Manage Category</h2>
          </div>
        </div>      
      </div>
      <div class="container">
        <div class="row">

          <div class="col-md-12 ftco-animate">
            <div class="cart-list">
              <table class="table">
                <thead class="thead-primary">
                  <tr class="text-center">
                    <th>&nbsp;</th>
                    <th>&nbsp;</th>
                    <!-- <th>&nbsp;</th> -->
										<th>Custom Category</th>
                    <th>Category Name</th>
                    <th>Description</th>
                    
                    <th>&nbsp;</th>
                  </tr>
                </thead>
                <?php

                 include '../db.php';
                  $queryfirst = "SELECT * FROM category";
                $resultfirst = $connection->query($queryfirst);
                if (isset($resultfirst->num_rows)) {
                  while($rowfirst = $resultfirst->fetch_assoc()) {

                        $id_best = $rowfirst['Id'];
                        $CustomCategory = $rowfirst['CustomCategory'];
												$thumbnail = $rowfirst['categoryImage'];
                        $Description = $rowfirst['Description'];
                        $name = $rowfirst['name'];
           
                        
           

            ?>
                <tbody>

                  <tr class="text-center">
                   
                    <td class="product-remove"><a href="CategoryRemove.php/?id=<?=  $id_best ?>"><span class="ion-ios-close"></span></a></td>

                    <td class="image-prod"><div class="img" style="background-image:url(../../images/category/<?= $thumbnail ?>);"></div></td>
                    
                    
                    <td class="CustomCategory">
                      <h3><?= $CustomCategory ?></h3>
                    </td>
										
                    <td class="Name">
                      <h3><?= $name ?></h3>
                    </td>
                    <td class="Description" style="width: 350px">
                      <h3><?= $Description ?></h3>
                    </td>
                    
                    
                    <td class="Edit"><a href="CategoryEdit.php?Id=<?= $id_best; ?>" class="btn-Success">Edit</a></td>
                    <td></td>
                    
                    
                  </tr>

                  
                </tbody>
                <?php
                }} ?>
              </table>
            </div>
          </div>
           
        </div>
       
      </div>
    </section>

    



    

    
    <?php  require '../footer.php'; ?>
  
    
  </body>
</html>