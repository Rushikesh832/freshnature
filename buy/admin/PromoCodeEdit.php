<?php session_start();
require '../headers.php';

if (!isset($_SESSION['login'])) {
    header('Location: ../../signIn.php');
}

if ($_SESSION['role'] != 'admin') {
    header('Location: ../../index.php');

}

$id =$_GET['Id'];



include '../db.php';
$queryfirst = "select * from promo_code";
// echo $queryfirst;die;
$resultfirst = $connection->query($queryfirst);
if (isset($resultfirst->num_rows)) {
    while($rowfirst = $resultfirst->fetch_assoc()) {

        $Id = $rowfirst['Id'];
        $code = $rowfirst['Code'];
        $Description = $rowfirst['Description'];
        $starting_date = $rowfirst['starting_date'];
        $percentage_discount = $rowfirst['percentage_discount'];
        $no_of_days = $rowfirst['no_of_days'];
        $flat_discount = $rowfirst['flat_discount'];
        $discount_upto = $rowfirst['discount_upto'];
        $min_amount = $rowfirst['min_amount'];
        $no_of_users = $rowfirst['no_of_users'];
        $status = $rowfirst['status'];





    }
}

?>
<style type="text/css">
    hr {
        border-style: double;
        border-width: 2px;
    }
</style>
<style>
    .switch {
        position: relative;
        display: inline-block;
        width: 60px;
        height: 34px;
    }

    .switch input {
        opacity: 0;
        width: 0;
        height: 0;
    }

    .slider {
        position: absolute;
        cursor: pointer;
        top: 0;
        left: 0;
        right: 0;
        bottom: 0;
        background-color: #ccc;
        -webkit-transition: .4s;
        transition: .4s;
    }

    .slider:before {
        position: absolute;
        content: "";
        height: 26px;
        width: 26px;
        left: 4px;
        bottom: 4px;
        background-color: white;
        -webkit-transition: .4s;
        transition: .4s;
    }

    input:checked + .slider {
        background-color: #2196F3;
    }

    input:focus + .slider {
        box-shadow: 0 0 1px #2196F3;
    }

    input:checked + .slider:before {
        -webkit-transform: translateX(26px);
        -ms-transform: translateX(26px);
        transform: translateX(26px);
    }

    /* Rounded sliders */
    .slider.round {
        border-radius: 34px;
    }

    .slider.round:before {
        border-radius: 50%;
    }
</style>

<hr size="2px">
<!DOCTYPE html>
<html lang="en">


<section class="ftco-section">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-xl-7 ftco-animate">
                <form action="updatePromoCodeData.php/?id=<?php echo $id?>" method="post"  enctype="multipart/form-data">
                    <h3 class="mb-4 billing-heading">Update Promo Code</h3>
                    <div class="row align-items-end">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="code"> Promo Code Name</label>
                                <input type="text" name="code" class="form-control" value="<?php  echo $code;?>" required="">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="Promo Code-image">Status Disabled/Enabled</label>&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
                                <label class="switch">
                                    <?php
                                    if ($status == 1)
                                    { ?>

                                        <input type="checkbox"  name ="status" checked>
                                        <span class="slider round"></span>
                                    <?php }
                                    elseif ($status == 0)
                                    { ?>

                                        <input type="checkbox"  name ="status" unchecked>
                                        <span class="slider round"></span>
                                    <?php }
                                    else{ ?>
                                        <input type="checkbox"  name ="status" unchecked>
                                        <span class="slider round"></span>
                                    <?php }?>
                                </label>
                            </div>
                        </div>


                        <div class="w-100"></div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="country">Description</label>
                                <input type="text"  name="description" class="form-control" value="<?php  echo $Description;?>"  >
                            </div>
                        </div>

                        <div class="w-100"></div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="StartingDate">Starting Date</label>
                                <input type="text" class="form-control" name="startingDate" value="<?php  echo $starting_date;?>"  required="">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="validUpto">Valid Upto(in Days)</label>
                                <input type="text" class="form-control" name="validUpto" value="<?php  echo $no_of_days;?>" required>
                            </div>
                        </div>




                        <div class="form-group">
                            <div class="col-md-3">
                                <div class="radio">
                                    <label><input type="radio" name="promotype"  class="mr-2" value="PercentageDiscount" > Percentage Discount</label>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-3">
                                <div class="radio">
                                    <label><input type="radio" name="promotype"  class="mr-2" value="FlatOff" > Flat Off</label>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-3">
                                <div class="radio">
                                    <label><input type="radio" name="promotype"  class="mr-2" value="FirstUsers"> First Users</label>
                                </div>
                            </div>
                        </div>



                        <div   class="PercentageDiscount select" >
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="Discount"> Discount(In %)</label>
                                    <input type="text" class="form-control" name="DiscountP" value="<?php  echo $percentage_discount;?>" >
                                </div>
                            </div>

                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="DiscountUpto"> Discount Upto(In ₹)</label>
                                    <input type="text" class="form-control" name="DiscountUpto" value="<?php  echo $discount_upto;?>">
                                </div>
                            </div>


                            <div class="form-group">
                                <div class="col-md-3">
                                    <div class="radio">
                                        <label><input type="radio" name="usertype"  class="mr-2" value="SpecificUsers">Specific Users</label>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-md-3">
                                    <div class="radio">
                                        <label><input type="radio" name="usertype"  class="mr-2" value="AllUsers">For All Users</label>
                                    </div>
                                </div>
                            </div>


                        </div>






                        <div   class="FlatOff select" >

                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="Discount">Flat Discount(In ₹)</label>
                                    <input type="text" class="form-control" name="DiscountF" value="<?php  echo $flat_discount;?>" >
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-md-3">
                                    <div class="radio">
                                        <label><input type="radio" name="usertype"  class="mr-2" value="SpecificUsers" >Specific Users</label>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-md-3">
                                    <div class="radio">
                                        <label><input type="radio" name="usertype"  class="mr-2" value="AllUsers" >For All Users</label>
                                    </div>
                                </div>
                            </div>


                        </div>








                        <div   class="FirstUsers select" >
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="Discount"> Number Of Users</label>
                                    <input type="text" class="form-control" name="NoOfUsers" value="<?php  echo $no_of_users;?>" >
                                </div>
                            </div>

                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="DiscountUpto"> Discount Upto(In ₹)</label>
                                    <input type="text" class="form-control" name="DiscountUptoF" value="<?php  echo $discount_upto;?>" >
                                </div>
                            </div>


                        </div>

                        </div>

                        <div class="w-100"></div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="MinAmount">Minimum Amount</label>
                                <input type="text" class="form-control" name="minamount"  value="<?php  echo $min_amount;?>" required="">
                            </div>
                        </div>

                        <div class="w-100"></div>

                        <div class="col-md-12">
                            <div class="form-group mt-4">
                                <button type="submit" class="btn btn-success"  name ="addPromoCode" style="width: 120px; height: 38px">Update Promo Code</button>

                            </div>
                        </div>
                </form><!-- END -->
                    </div>

            </div>
        </div>
    </div>
</section> <!-- .section -->

<?php

require '../footer.php'; ?>

</body>
</html>


<script type="text/javascript">
    $(document).ready(function() {
        $(".select").hide();
        $('input[type="radio"]').click(function() {
            var inputValue = $(this).attr("value");
            var targetBox = $("." + inputValue);
            $(".select").not(targetBox).hide();
            $(targetBox).show();
        });
    });


</script>