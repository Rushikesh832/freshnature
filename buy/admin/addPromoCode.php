<?php session_start();
require '../headers.php';


if (!isset($_SESSION['login'])) {
    header('Location:../../signIn.php');
}

if ($_SESSION['role'] != 'admin') {
    header('Location:../../index.php');

}

?>
<style type="text/css">
    hr {
        border-style: double;
        border-width: 2px;
    }
</style>
<hr size="2px">
<!DOCTYPE html>
<html lang="en">

<section class="ftco-section">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-xl-7 ftco-animate">
                <form action="addPromoCodeData.php" method="post"  enctype="multipart/form-data">
                    <h3 class="mb-4 billing-heading">Add Promo Code</h3>
                    <div class="row align-items-end">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="PromoCodeName"> Promo Code Name</label>
                                <input type="text" name="code" class="form-control" placeholder="Enter PromoCode Name " required="">
                            </div>
                        </div>


                        <!--<div class="col-md-6">
                            <div class="form-group">
                                <label for="category">Category</label></br>
                                <select name="category" style="width: 300px;" class="form-control">
                                    <?php
/*                                    include '../db.php';
                                    $queryfirst = "SELECT * FROM category";
                                    $resultfirst = $connection->query($queryfirst);
                                    if (isset($resultfirst->num_rows)) {
                                        while($rowfirst = $resultfirst->fetch_assoc()) {
                                            $name_best = $rowfirst['name'];
                                            $id_best = $rowfirst['Id'];
                                            $CustomCategory = $rowfirst['CustomCategory'];

                                            */?>

                                            <option value="<?/*= $CustomCategory*/?>"><?/*= $name_best*/?></option>
                                        <?php /* }} */?>
                                </select>
                            </div>
                        </div>-->



                        <div class="w-100"></div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="country">Description(optional)</label>
                                <input type="text"  name="description" class="form-control" placeholder="Description" >
                            </div>
                        </div>

                        <div class="w-100"></div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="StartingDate">Starting Date</label>
                                <input type="date" class="form-control" name="Sdate" placeholder=" Starting Date" required="">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="ValidUpto">Valid Upto(In Days)</label>
                                <input type="text" class="form-control" name="validupto" placeholder="Enter Validity" required="">
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-3">
                                <div class="radio">
                                    <label><input type="radio" name="promotype"  class="mr-2" value="PercentageDiscount" > Percentage Discount</label>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-3">
                                <div class="radio">
                                    <label><input type="radio" name="promotype" class="mr-2" value="FlatOff" > Flat Off</label>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-3">
                                <div class="radio">
                                    <label><input type="radio" name="promotype"  class="mr-2" value="FirstUsers"> First Users</label>
                                </div>
                            </div>
                        </div>



                        <div   class="PercentageDiscount select" >
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="Discount"> Discount(In %)</label>
                                    <input type="text" class="form-control" name="DiscountP" placeholder="Enter Discount" >
                                </div>
                            </div>

                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="DiscountUpto"> Discount Upto(In ₹)</label>
                                    <input type="text" class="form-control" name="DiscountUpto" placeholder="Enter Discount" >
                                </div>
                            </div>


                            <div class="form-group">
                                <div class="col-md-3">
                                    <div class="radio">
                                        <label><input type="radio" name="usertype"  class="mr-2" value="SpecificUsers">Specific Users</label>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-md-3">
                                    <div class="radio">
                                        <label><input type="radio" name="usertype"  class="mr-2" value="AllUsers">For All Users</label>
                                    </div>
                                </div>
                            </div>


                        </div>






                        <div   class="FlatOff select" >

                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="Discount">Flat Discount(In ₹)</label>
                                    <input type="text" class="form-control" name="DiscountF" placeholder="Enter Discount" >
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-md-3">
                                    <div class="radio">
                                        <label><input type="radio" name="usertype"  class="mr-2" value="SpecificUsers" >Specific Users</label>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-md-3">
                                    <div class="radio">
                                        <label><input type="radio" name="usertype"  class="mr-2" value="AllUsers" >For All Users</label>
                                    </div>
                                </div>
                            </div>


                        </div>








                        <div   class="FirstUsers select" >
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="Discount"> Number Of Users</label>
                                    <input type="text" class="form-control" name="NoOfUsers" placeholder="Enter Discount" >
                                </div>
                            </div>

                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="DiscountUpto"> Discount Upto(In ₹)</label>
                                    <input type="text" class="form-control" name="DiscountUptoF" placeholder="Enter Discount" >
                                </div>
                            </div>


                        </div>










                        <div class="w-100"></div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="MinAmount">Minimum Amount</label>
                                <input type="text" class="form-control" name="minamount" placeholder="Enter Minimum Amount" required="">
                            </div>
                        </div>
                        <div class="w-100"></div>
                        </br>

                        <div class="col-md-12">
                            <div class="form-group mt-4">
                                <button type="submit" class="btn btn-success"  name ="addPromoCode" style="width: 120px; height: 38px">Add Promo Code</button>

                            </div>
                        </div>
                    </div>
                </form><!-- END -->
            </div>
        </div>
    </div>
</section> <!-- .section -->

<?php

require '../footer.php'; ?>

</body>
</html>


<script type="text/javascript">
    $(document).ready(function() {
        $(".select").hide();
        $('input[type="radio"]').click(function() {
            var inputValue = $(this).attr("value");
            var targetBox = $("." + inputValue);
            $(".select").not(targetBox).hide();
            $(targetBox).show();
        });
    });


</script>