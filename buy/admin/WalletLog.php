<?php session_start();

if (!isset($_SESSION['login']))
{
    header('Location: signIn.php');
}
if ($_SESSION['role'] != 'admin') {
    header('Location: ../../index.php');

}
require '../headers.php';

$id =$_GET['Id'];


?>

<!DOCTYPE html>
<html lang="en">



<section class="ftco-section ftco-cart">
    <div class="container">
        <div class="row justify-content-center mb-3 pb-3">
            <div class="col-md-12 heading-section text-center ftco-animate">
                <h2 class="mb-4">User Wallet</h2>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row">

            <div class="col-md-12 ftco-animate">
                <div class="cart-list">
                    <table class="table">
                        <theadlle class="thead-primary">
                        <tr class="text-center">

                            <th>User Mail</th>
                            <th>User Phone Number</th>
                            <th>Current Wallet Balance</th>
                            <th>Description</th>
                            <th>Amount</th>
                            <th>Status</th>
                            <th>Received/Paid</th>
                            <th>Order Number</th>
                            <th>Transaction Date</th>

                        </tr>
                        </theadlle>
                        <?php

                        include '../db.php';
                        $queryfirst = "SELECT * FROM UserWallet where Userid = $id";
                        $resultfirst = $connection->query($queryfirst);
                        if (isset($resultfirst->num_rows)) {
                            while($rowfirst = $resultfirst->fetch_assoc()) {

                                $id_best = $rowfirst['Id'];
                                $UserId = $rowfirst['Userid'];
                                $currentBalance = $rowfirst['currentBalance'];
                                $Description = $rowfirst['description'];
                                $Amount = $rowfirst['Amount'];
                                $status = $rowfirst['status'];
                                $R_P = $rowfirst['Received_Paid'];
                                $OrderId = $rowfirst['OrderId'];
                                $Date = $rowfirst['TransactionDate'];

                        $queryfirst = "SELECT * FROM users where Id=$UserId";
                        $resultfirst = $connection->query($queryfirst);
                        if (isset($resultfirst->num_rows)) {
                            while($rowfirst1 = $resultfirst->fetch_assoc()) {
                                $UserMail= $rowfirst1['email'];
                                $UserPhone= $rowfirst1['phoneNumber'];

                            }
                        }



                                ?>
                                <tbody>

                                <tr class="text-center">



                                    <td class="email">
                                        <h3><?= $UserMail ?></h3>
                                    </td>
                                    <td class="PhoneN" style="width: 350px">
                                        <h3><?= $UserPhone ?></h3>
                                    </td>
                                    <td class="currentBalance">
                                        <h3>₹<?= $currentBalance ?></h3>
                                    </td>
                                    <td class="Description" style="width: 350px">
                                        <h3><?= $Description ?></h3>
                                    </td>
                                    <td class="Amount">
                                        <h3>₹<?= $Amount ?></h3>
                                    </td>
                                    <td class="status" style="width: 350px">
                                        <h3><?= $status ?></h3>
                                    </td>
                                    <td class="receive/paid">
                                        <h3><?= $R_P ?></h3>
                                    </td>
                                    <td class="OrderNumber" style="width: 350px">
                                        <h3><?= $OrderId ?></h3>
                                    </td>
                                    <td class="transactionDate" style="width: 350px">
                                        <h3><?= $Date ?></h3>
                                    </td>


                                </tr>


                                </tbody>
                                <?php
                            }} ?>
                    </table>
                </div>
            </div>

        </div>

    </div>
</section>








<?php  require '../footer.php'; ?>


</body>
</html>