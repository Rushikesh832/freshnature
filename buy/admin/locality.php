<?php session_start();

if ($_SESSION['role'] != 'admin') {
  header('Location: ../../index.php');

}
if (!isset($_SESSION['login'])) {
    header('Location: ../../signIn.php');
  }

 require "../headers.php";
 ?>

 <section class="ftco-section">
      <div class="container">
        <div class="row justify-content-center mb-3 pb-3">
          <div class="col-md-12 heading-section text-center ftco-animate">
            <h2 class="mb-4">Locality</h2>
          </div>
        </div>      
      </div>
      <div class="container">
        <div class="row">
          <div class="col-md-6 col-lg-3 ftco-animate">
            <div class="product">
              <a href="addLocality.php" class="img-prod"><img class="img-fluid" src="../images/add.png" alt="Colorlib Template">
                <div class="overlay"></div>
              </a>
              <div class="text py-3 pb-4 px-3 text-center">
                <h3><a href="addLocality.php">Add Locality</a></h3>
                </div>
            </div>
          </div>
           <div class="col-md-6 col-lg-3 ftco-animate">
            <div class="product">
              <a href="showLocality.php" class="img-prod"><img class="img-fluid" src="../images/Showing.jpg" alt="Colorlib Template">
                <div class="overlay"></div>
              </a>
              <div class="text py-3 pb-4 px-3 text-center">
                <h3><a href="showLocality.php">Manage Locality</a></h3>
                </div>
            </div>
          </div>

          
        </div>
      </div>
    </section>

 
 <?php require '../footer.php'; ?>
