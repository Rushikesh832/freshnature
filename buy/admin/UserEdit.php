<?php session_start();
  require '../headers.php'; 

 if (!isset($_SESSION['login'])) {
    header('Location: ../../signIn.php');
  }

  if ($_SESSION['role'] != 'admin') {
  header('Location: ../../index.php');

}

$id =$_GET['Id'];


           include '../db.php';
                  $queryfirst = "SELECT * FROM users where Id=$id ";
                $resultfirst = $connection->query($queryfirst);
                if (isset($resultfirst->num_rows)) {
                  while($rowfirst = $resultfirst->fetch_assoc()) {

                        $id_best = $rowfirst['Id'];
                        $user_firstname = $rowfirst['firstname'];
                        $user_lastname = $rowfirst['lastname'];
                        $user_email = $rowfirst['email'];
                        $user_address = $rowfirst['address'];
                        $user_country = $rowfirst['country'];
                        $user_city = $rowfirst['city'];
           
                        
           

            }
          }
  
  
 ?>
 <!DOCTYPE html>
<html lang="en">
  
    
    <section class="ftco-section">
      <div class="container">
        <div class="row justify-content-center">
          <div class="col-xl-7 ftco-animate">
             <form action="UpdateUserData.php/?id=<?php echo $id?>" method="post"  enctype="multipart/form-data">
              <h3 class="mb-4 billing-heading">Update User Profile</h3>
              <div class="row align-items-end">
                <div class="col-md-6">
                  <div class="form-group">
                    <label for="firstname">First Name</label>
                    <input type="text" name="firstname" class="form-control"  required="" value="<?php echo $user_firstname?>"  >
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="form-group">
                    <label for="lastname">Last Name</label>
                    <input type="text"  name="lastname" class="form-control" value="<?php echo $user_lastname?>"  required="">
                  </div>
                </div>

                <div class="form-group">
                    <div class="col-md-3">
                      <div class="radio">
                         <label><input type="radio" name="role" class="mr-2" value="admin"> admin</label>
                      </div>
                    </div>
                  </div>
                  
                  <div class="form-group">
                    <div class="col-md-3">
                      <div class="radio">
                         <label><input type="radio" name="role"  class="mr-2" value="client"> client</label>
                      </div>
                    </div>
                  </div>

                  
                <div class="w-100"></div>
                <div class="col-md-12">
                  <div class="form-group">
                    <label for="streetaddress">Street Address</label>
                    <input type="text" class="form-control" name="address" value="<?php echo $user_address?>"   >
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="form-group">
                    <label for="country">Country</label>
                    <input type="text"  name="country" class="form-control" value="<?php echo $user_country?>"  >
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="form-group">
                    <label for="towncity">Town / City</label>
                    <input type="text" class="form-control" name="city" value="<?php echo $user_city?>" >
                  </div>
                </div>

                 <div class="form-group">
                    <div class="col-md-3">
                      <div class="radio">
                         <label><input type="radio" name="addresstype"  class="mr-2" value="home"> Home</label>
                      </div>
                    </div>
                  </div>
                  
                  <div class="form-group">
                    <div class="col-md-3">
                      <div class="radio">
                         <label><input type="radio" name="addresstype"  class="mr-2" value="work"> Work</label>
                      </div>
                    </div>
                  </div>

                  <div class="form-group">
                    <div class="col-md-3">
                      <div class="radio">
                         <label><input type="radio" name="addresstype"  class="mr-2" value="other"> Other</label>
                      </div>
                    </div>
                  </div>
                <div class="w-100"></div>
                <div class="col-md-12">
                  <div class="form-group mt-4">
                  <button type="submit" class="btn btn-info"  name ="signUp" style="width: 120px; height: 38px"> Update Profile</button>
                  </div>
                </div>
              </div>
            </form><!-- END -->
          </div>
          </div>
          
        </div>
      </div>
    </section> <!-- .section -->

    
    <?php  require '../footer.php'; ?>
  
    
  </body>
</html>