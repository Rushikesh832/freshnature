<?php session_start();

if (!isset($_SESSION['login']))
{
    header('Location: signIn.php');
}
if ($_SESSION['role'] != 'admin') {
    header('Location: ../../index.php');

}
require '../headers.php';




?>

<!DOCTYPE html>
<html lang="en">



<section class="ftco-section ftco-cart">
    <div class="container">
        <div class="row justify-content-center mb-3 pb-3">
            <div class="col-md-12 heading-section text-center ftco-animate">
                <h2 class="mb-4">Manage Delivery Boy</h2>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row">

            <div class="col-md-12 ftco-animate">
                <div class="cart-list">
                    <table class="table">
                        <thead class="thead-primary">
                        <tr class="text-center">
                            <th>E-mail</th>
                            <th>Phone Number</th>
                            <th>First Name</th>
                            <th>Last Name</th>
                            <th>Locality</th>
                            <th> Address</th>
                            <th>City</th>

                            <th>&nbsp;</th>
                        </tr>
                        </thead>
                        <?php

                        include '../db.php';
                        $queryfirst = "SELECT * FROM delivery_boy";
                        $resultfirst = $connection->query($queryfirst);
                        if (isset($resultfirst->num_rows)) {
                            while($rowfirst = $resultfirst->fetch_assoc()) {


                                $Id = $rowfirst['Id'];
                                $email = $rowfirst['email'];
                                $firstname = $rowfirst['first_name'];
                                $lastname = $rowfirst['last_name'];
                                $address = $rowfirst['address'];
                                $phoneNumber = $rowfirst['phone_number'];
                                $city = $rowfirst['city'];
                                $Locality = $rowfirst['locality'];





                                ?>
                                <tbody>

                                <tr class="text-center" >

                                    <td class="email" style="width:50px">
                                        <h3 ><?= $email ?></h3>
                                    </td>

                                    <td class="phoneNumber" style="width:50px">
                                        <h3 ><?= $phoneNumber ?></h3>
                                    </td>



                                    <td class="firstname">
                                        <h3><?= $firstname ?></h3>
                                    </td><td class="lastname">
                                        <h3><?= $lastname ?></h3>
                                    </td><td class="address">
                                        <h3><?= $address ?></h3>


                                    <td class="city">
                                        <h3><?= $city ?></h3>
                                    </td>

                                    <td class="Loacality">
                                        <h3><?= $Locality ?></h3>
                                    </td>


                                    <td class="Edit"><a href="DeliveyBoyEdit.php?Id=<?= $Id; ?>" class="btn-Success">Edit</a></td>
                                    <td></td>


                                </tr>


                                </tbody>
                                <?php
                            }} ?>
                    </table>
                </div>
            </div>

        </div>

    </div>
</section>








<?php  require '../footer.php'; ?>


</body>
</html>