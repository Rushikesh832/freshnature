<?php session_start();
if (!isset($_SESSION['login']))
{
    header('Location:signIn.php');
}

require '../headers.php';

?>
<!DOCTYPE html>
<html lang="en">
<div class="hero-wrap hero-bread" style="background-image: url('../../images/banner/bg_1.jpg');">
    <div class="container">
        <div class="row no-gutters slider-text align-items-center justify-content-center">
            <div class="col-md-9 ftco-animate text-center">
                <p class="breadcrumbs"><span class="mr-2"><a href="index.html">Home</a></span> <span>Password</span></p>
                <h1 class="mb-0 bread">Change Password</h1>
            </div>
        </div>
    </div>
</div>

<section class="ftco-section">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-xl-7 ftco-animate">
                <form action="UpdatePassword.php" method="post"  enctype="multipart/form-data">
                    <div style="margin-left: 3%; color: #00ff25; font-size: 2vw;" >
                        <?php
                        if (isset($_GET['message']))
                        {
                            echo $_GET['message'];
                        }
                        ?>
                    </div>
                    <div class="row align-items-end">

                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="Old_Password">Old Password</label>
                                <input type="text" name="oldPassword" class="form-control"  required="" placeholder="Enter Your Old Password..."  >
                            </div>
                        </div>

                        <div class="w-100"></div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="newPassword">New Password</label>
                                <input type="text"  name="newPassword" class="form-control" placeholder="Enter New Password..." required="" >
                            </div>
                        </div>

                        <div class="w-100"></div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="ConfirmPassword">Confirm Password</label>
                                <input type="text" class="form-control" name="confirmPassword" placeholder="Confirm Password..." required="" >
                            </div>
                        </div>


                        <div class="w-100"></div>
                        <div class="col-md-12">
                            <div class="form-group mt-4">
                                <button type="submit" class="btn btn-info"  name ="ResetPassword" style="width: 120px; height: 38px"> Update Password</button>
                            </div>
                        </div>

                </form><!-- END -->
            </div>
        </div>

    </div>
    </div>
</section> <!-- .section -->


<?php  require '../footer.php'; ?>


</body>
</html>