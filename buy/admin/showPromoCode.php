<?php session_start();

if (!isset($_SESSION['login']))
{
    header('Location: signIn.php');
}
if ($_SESSION['role'] != 'admin') {
    header('Location: ../../index.php');

}
require '../headers.php';




?>

<!DOCTYPE html>
<html lang="en">



<section class="ftco-section ftco-cart">
    <div class="container">
        <div class="row justify-content-center mb-3 pb-3">
            <div class="col-md-12 heading-section text-center ftco-animate">
                <h2 class="mb-4">All Promo Code</h2>
                <div style="text-align: right;">
                    <input class="form-control" id="myInput" style="width: 200px;outline-style: inset" type="text" placeholder="Search...">
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row">

            <div class="col-md-12 ftco-animate">
                <div class="cart-list">
                    <table class="table">
                        <thead class="thead-primary">
                        <tr class="text-center">

                            <th>Promo Code</th>
                            <th>Starting Date</th>
                            <th>Valid Upto</th>
                            <th>Description</th>

                            <th> Percentage Discount</th>
                            <th> Flat Discount</th>
                            <th>Discount Upto</th>
                            <th> Minimum Amount</th>
                            <th>User List</th>


                            <th> Status</th>
                            <th>Admin Email</th>

                            <th>Edit</th>


                        </tr>
                        </thead>
                        <?php

                        include '../db.php';
                        $queryfirst = "SELECT * FROM promo_code, users where users.Id=promo_code.admin_id";
                        // echo $queryfirst;
                        $resultfirst = $connection->query($queryfirst);
                        if (isset($resultfirst->num_rows)) {
                            while($rowfirst = $resultfirst->fetch_assoc()) {



                                $Id = $rowfirst['Id'];
                                $code = $rowfirst['Code'];
                                $Description = $rowfirst['Description'];
                                $starting_date = $rowfirst['starting_date'];
                                $percentage_discount = $rowfirst['percentage_discount'];
                                $no_of_days = $rowfirst['no_of_days'];
                                $flat_discount = $rowfirst['flat_discount'];
                                $discount_upto = $rowfirst['discount_upto'];
                                $min_amount = $rowfirst['min_amount'];
                                $no_of_users = $rowfirst['no_of_users'];
                                $status = $rowfirst['status'];
                                $email = $rowfirst['email'];

                                ?>
                                <tbody id="myTable">

                                <tr class="text-center" >


                                    <td class="code" style="width:50px">
                                        <h3 ><?= $code ?></h3>
                                    </td>
                                    <td class="starting_date">
                                        <h3><?= $starting_date ?></h3>
                                    </td>
                                    <td class="no_of_days">
                                        <h3><?= $no_of_days ?></h3>
                                    </td>

                                    <td class="Description">
                                        <h3><?= $Description ?></h3>
                                    </td>


                                    <td class="percentage_discount">
                                        <h3><?= $percentage_discount ?></h3>
                                    </td>
                                    <td class="flat_discount">
                                        <h3><?= $flat_discount ?></h3>
                                    </td>

                                    <td class="discount_upto">
                                        <h3><?= $discount_upto ?></h3>
                                    </td><td class="min_amount">
                                        <h3><?= $min_amount ?></h3>

                                    </td><td class="no_of_users">
                                        <h3><?= $no_of_users ?></h3>
                                    </td><td class="status">
                                        <h3><?= $status ?></h3>
                                    </td>
                                    <td class="email">
                                        <h3><?= $email ?></h3>
                                    </td>

                                    <td class="Preview"><a href="PromoCodeEdit.php?Id=<?= $Id; ?>" class="btn-Success">Edit</a></td>



                                    <td></td>


                                </tr>


                                </tbody>
                                <?php
                            }} ?>
                    </table>
                </div>
            </div>

        </div>

    </div>
</section>








<?php  require '../footer.php'; ?>


</body>
</html>

<script>
    $(document).ready(function(){
        $("#myInput").on("keyup", function() {
            var value = $(this).val().toLowerCase();
            $("#myTable tr").filter(function() {
                $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
            });
        });
    });
</script>