<?php session_start();
require '../headers.php'; 

 if (!isset($_SESSION['login'])) {
    header('Location: ../../signIn.php');
  }

  if ($_SESSION['role'] != 'admin') {
  header('Location: ../../index.php');

}

$id =$_GET['Id'];



                 include '../db.php';
                  $queryfirst = "SELECT * FROM Gallary where Id=$id ";
                $resultfirst = $connection->query($queryfirst);
                if (isset($resultfirst->num_rows)) {
                  while($rowfirst = $resultfirst->fetch_assoc()) {

                        $id_best = $rowfirst['Id'];
                        $name_best = $rowfirst['name'];
                        $Description = $rowfirst['Description'];
           
                        
           

            }
          }
  
?>
<!DOCTYPE html>
<html lang="en">
  <style type="text/css">
  hr {
  border-style: double;
  border-width: 2px;
}
</style>
  <hr size="2px">

    <section class="ftco-section">
      <div class="container">
        <div class="row justify-content-center">
          <div class="col-xl-7 ftco-animate">
            <form action="updateGallaryData.php/?id=<?php echo $id_best?>" method="post"  enctype="multipart/form-data">
              <h3 class="mb-4 billing-heading">Update Gallary Data</h3>
              <div class="row align-items-end">
                <div class="col-md-6">
                  <div class="form-group">
                    <label for="firstname"> Gallary Name</label>
                    <input type="text" name="name" class="form-control" value="<?php  echo $name_best;?>" required="">
                  </div>
                </div>
                
                <div class="w-100"></div>
                <div class="col-md-12">
                  <div class="form-group">
                    <label for="country">Description</label>
                    <input type="text"  name="description" class="form-control" value="<?php  echo $Description;?>"  required="">
                  </div>
                </div>
                
                
                
                <div class="w-100"></div>

                 <div class="col-md-6">
                  <div class="form-group">
                    <label for="product-image">Gallary Image</label>
                    <input type="file" class="form-control"  name="thumbnail" placeholder="">
                  </div>
                </div>
               
                <div class="w-100"></div>
                </br>

                <div class="col-md-12">
                  <div class="form-group mt-4">
                  <button type="submit" class="btn btn-success"  name ="addgallary" style="width: 120px; height: 38px">Update Gallary</button>
                 
                  </div>
                </div>
              </div>
            </form><!-- END -->
          </div>
        </div>
      </div>
    </section> <!-- .section -->

    <?php  
        
    require '../footer.php'; ?>
    
  </body>
</html>