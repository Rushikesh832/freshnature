<?php session_start();
require '../headers.php';

if (!isset($_SESSION['login'])) {
    header('Location: ../../signIn.php');
}

if ($_SESSION['role'] != 'admin') {
    header('Location: ../../index.php');

}

$id =$_GET['Id'];


include '../db.php';
$queryfirst = "SELECT * FROM delivery_boy where Id=$id ";
$resultfirst = $connection->query($queryfirst);
if (isset($resultfirst->num_rows)) {
    while($rowfirst = $resultfirst->fetch_assoc()) {

        $Id = $rowfirst['Id'];
        $email = $rowfirst['email'];
        $firstname = $rowfirst['first_name'];
        $lastname = $rowfirst['last_name'];
        $address = $rowfirst['address'];
        $phoneNumber = $rowfirst['phone_number'];
        $city = $rowfirst['city'];
        $Locality = $rowfirst['locality'];

    }
}


?>
<!DOCTYPE html>
<html lang="en">


<section class="ftco-section">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-xl-7 ftco-animate">
                <form action="UpdateDBData.php/?id=<?php echo $id?>" method="post"  enctype="multipart/form-data">
                    <h3 class="mb-4 billing-heading">Update Delivery Boy Profile</h3>
                    <div class="row align-items-end">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="firstname">First Name</label>
                                <input type="text" name="firstname" class="form-control"  required="" value="<?php echo $firstname?>"  >
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="lastname">Last Name</label>
                                <input type="text"  name="lastname" class="form-control" value="<?php echo $lastname?>"  required="">
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="E-mail">E-Mail</label>
                                <input type="text" name="email" class="form-control"  required="" value="<?php echo $email?>"  >
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="PhoneNumber">Phone Number</label>
                                <input type="text"  name="phonenumber" class="form-control" value="<?php echo $phoneNumber?>"  required="">
                            </div>
                        </div>


                        <div class="w-100"></div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="streetaddress">Street Address</label>
                                <input type="text" class="form-control" name="address" value="<?php echo $address?>"   >
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="towncity">Town / City</label>
                                <input type="text" class="form-control" name="city" value="<?php echo $city?>" >
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="Locality">Locality</label>
                                <input type="text" class="form-control" name="locality" value="<?php echo $Locality?>" >
                            </div>
                        </div>


                        <div class="w-100"></div>
                        <div class="col-md-12">
                            <div class="form-group mt-4">
                                <button type="submit" class="btn btn-info"  name ="signUp" style="width: 120px; height: 38px"> Update Profile</button>
                            </div>
                        </div>
                    </div>
                </form><!-- END -->
            </div>
        </div>

    </div>
    </div>
</section> <!-- .section -->


<?php  require '../footer.php'; ?>


</body>
</html>